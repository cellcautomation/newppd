package gui.Functions;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.net.InetAddress;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.SAXException;
import org.apache.poi.EncryptedDocumentException;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.hssf.*;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.ExtentTest;
import com.codoid.products.exception.FilloException;
import com.codoid.products.fillo.Connection;
import com.codoid.products.fillo.Fillo;
import com.codoid.products.fillo.Recordset;
//import com.mysql.cj.jdbc.DatabaseMetaData;

public class DataFunctions {
	private String sWebURL;
	private String sWebPassword;
	private String sWebUsername;
	private String sAppLoc;
	private String sAppUsername;
	private String sAppPassword;
	private String sApi;
	private String sOutput;
	private String sDataLocation;
	private String sDataType;
	private String sDBPassword;
	private String sDBUsername;
	private String sEmail;
	private String sEmailFrom;
	private String sEmailTo;
	private String sDBHost;
	private String strproducttype;
	private String strmsisdnsts;
	public String outputValue;
	public String nrecord;
	public Connection connect;
	public java.sql.Connection conn = null;
	public Sheet sheet;
	public Workbook workbook;
	public String ListPricepalid1;
	// public String s;
	int col, Column_Count, Row_Count;
	int colnNum = 0;
	int fillonum = 1;
	public String PricePlanId;
	WebDriver driver;
	public int dealsize;
	public int prdtsize;
	public int pricesize;
	public int monthssiz;
	UtilityFunctions utils = new UtilityFunctions();
	ApplicationSpecific app = new ApplicationSpecific();

	// gdfhgh
	public String getWebURL() {
		return sWebURL;
	}

	public String getWebPassword() {
		return sWebPassword;
	}

	public String getWebUserName() {
		return sWebUsername;
	}

	public String getWindowsAppLocation() {
		return sAppLoc;
	}

	public String getWindowsAppUsername() {
		return sAppUsername;
	}

	public String getWindowsAppPassword() {
		return sAppPassword;
	}

	public String getAPI() {
		return sApi;
	}

	public String getDataLocation() {
		return sDataLocation;
	}

	public String getDBHost() {
		return sDBHost;
	}

	public String getDBUsername() {
		return sDBUsername;
	}

	public String getDBPassword() {
		return sDBPassword;
	}

	public String getDataType() {
		return sDataType;
	}

	public String getSendEmail() {
		return sEmail;
	}

	public String getEmailFrom() {
		return sEmailFrom;
	}

	public String getEmailTo() {
		return sEmailTo;
	}

	/*****************************************************************************
	 * Function Name: GetEnvironmentVariables Description: gets environment
	 * variables from the config json file Date Created: 13/09/2017
	 * 
	 * @param sDefaultPath
	 ******************************************************************************/
	public void GetEnvironmentVariables(String sDefaultPath, String Environment) throws IOException, ParseException {
		File f1 = null;
		FileReader fr = null;

		JSONParser parser = new JSONParser();
		try {
			f1 = new File(sDefaultPath + "\\conf\\" + Environment + ".json");
			fr = new FileReader(f1);
			Object obj = parser.parse(fr);
			JSONObject jsonObject = (JSONObject) obj;
			// System.out.print(jsonObject);
			// String[] env=new String[10];

			sWebURL = (String) jsonObject.get("weburl");
			sWebUsername = (String) jsonObject.get("webusername");
			sWebPassword = (String) jsonObject.get("webpassword");
			sAppLoc = (String) jsonObject.get("windowsapplocation");
			sAppUsername = (String) jsonObject.get("windowsappusername");
			sAppPassword = (String) jsonObject.get("windowsapppassword");
			sApi = (String) jsonObject.get("API");
			sDataLocation = (String) jsonObject.get("datalocation");
			sDataType = (String) jsonObject.get("datatype");
			sDBHost = (String) jsonObject.get("dbhost");
			sDBPassword = (String) jsonObject.get("dbpass");
			sDBUsername = (String) jsonObject.get("dbusername");
			sEmail = (String) jsonObject.get("sendemail");
			sEmailFrom = (String) jsonObject.get("emailfrom");
			sEmailTo = (String) jsonObject.get("emailto");

		} finally {
			try {
				fr.close();

			} catch (IOException ioe)

			{
				ioe.printStackTrace();
			}
		}

	}

	/*****************************************************************************
	 * Function Name: Date of Birth Function Description: Slects date on PPD
	 * Date Created: 26/04/2017
	 * 
	 * @param sDefaultPath
	 ******************************************************************************/

	/*****************************************************************************
	 * Function Name: WriteToTextFile Description: Write a value to a text file
	 * Date Created: 13/09/2017
	 ******************************************************************************/
	public void setDataOtput(String sOutput) {
		this.sOutput = sOutput;
	}

	public void WriteTextFile(String filePath) throws IOException {

		Writer output = null;
		File file = new File(filePath);
		output = new BufferedWriter(new FileWriter(file));

		String outputData = sOutput;
		output.write(outputData);
		output.write("\r\n");
		output.close();

	}

	public Sheet ReadExcel(String FILE_NAME, int i) throws IOException {
		FileInputStream excelFile = new FileInputStream(new File(FILE_NAME));
		workbook = new HSSFWorkbook(excelFile);
		sheet = workbook.getSheetAt(i);
		return sheet;
	}

	public String[] Existingcustomer(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet, Recordset record,
			ResultSet resultset, String Type, String sDefaultPath, String PPD, String location, String DataPath,
			int sheetnum) throws SAXException, IOException, ParserConfigurationException, Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord Found with the Msisdn You Have Searched" };
		String[] Completed = { "Completed" };

		String[] NOCustomer = { "NoCustomer Found with the Msisdn you have Searched" };
		// driver.switchTo().frame("topFrame").switchTo().frame("ifraTopMenu");

		driver.switchTo().frame("mainFrame");
		utils.EnterText(driver, "searchingfor", getCellData("Msisdn", iCount, sheet, record, resultset, Type),
				sDefaultPath + "\\Repository\\" + PPD + ".xml");
		utils.ClickObject(driver, "searchfor", sDefaultPath + "\\Repository\\" + PPD + ".xml");
		// boolean s=true;
		if (utils.checkIfObjectIsDisplayed(driver, "Norecord", sDefaultPath + "\\Repository\\" + PPD + ".xml")) {
			// utils.EnterText(driver, "username",
			// sUserName,sDefaultPath+"\\Repository\\"+PPD+".xml");
			// .EnterText(driver, "password",
			// sPass,sDefaultPath+"\\Repository\\"+PPD+".xml");
			WriteData(Results, iCount, sheetnum, NORecord, DataPath);
			WriteData(Results, iCount, sheetnum, Completed, DataPath);
			// s=false;
			// String str = String.valueOf(boovar);
			nrecord = "false";
			// driver.
			// utils.ClickObject(driver,
			// "buttonlogin",sDefaultPath+"\\Repository\\"+PPD+".xml");
			System.out.print("Login Successful");
		} else if (utils.checkIfObjectIsDisplayed(driver, "NoCustomer",
				sDefaultPath + "\\Repository\\" + PPD + ".xml")) {
			WriteData(Results, iCount, sheetnum, NOCustomer, DataPath);
			WriteData(Results, iCount, sheetnum, Completed, DataPath);
			Thread.sleep(2000);
			nrecord = "false";
		} else {
			System.out.print("Login was UnSuccessful");
			String strmsisdn = getCellData("Msisdn", iCount, sheet, record, resultset, Type);
			// utils.ClickObject(driver, "MsisdnListSearch",
			// sDefaultPath+"\\Repository\\"+PPD+".xml");
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(By.xpath("//table[@id='customerList']/tbody/tr/td[2]"));
			action.doubleClick(element).perform();
			Thread.sleep(3000);
			driver.switchTo().frame("rightTFrame");
			utils.ClickObject(driver, "subsriber", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			// .switchTo().parentFrame();
			Thread.sleep(2000);
			driver.switchTo().frame("iframeName");

			List<WebElement> Subsriberlistsize = driver
					.findElements(By.xpath("//tbody[@class='webfx-columnlist-body']/tr"));
			int subsize = Subsriberlistsize.size();

			// WebElement
			// element4=driver.findElement(By.xpath("//table[@id='subscriberInfoList']/tbody/tr"));
			outerloop: for (int ssize = 1; ssize <= subsize; ssize++) {
				List<WebElement> subdes = driver
						.findElements(By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + ssize + "]/td"));
				Thread.sleep(1000);

				for (int j = 0; j < subdes.size(); j++) {
					String yrmatch = subdes.get(j).getText();
					yrmatch = yrmatch.trim();
					strmsisdn = strmsisdn.trim();
					if (strmsisdn.equals(yrmatch)) {
						strmsisdnsts = driver
								.findElement(
										By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + ssize + "]/td[4]"))
								.getText();
						strproducttype = driver
								.findElement(
										By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + ssize + "]/td[2]"))
								.getText();
						// list_AllYearsmatch.get(yearmatch).click();
						strproducttype = strproducttype.trim();
						strmsisdnsts = strmsisdnsts.trim();
						break outerloop;

					}

				}
			}
		}

		String ar[] = new String[3];
		ar[0] = strmsisdnsts;
		ar[1] = strproducttype;
		ar[2] = nrecord;
		return ar;
	}

	public void windowhandler(WebDriver driver) {
		Set<String> handlers = driver.getWindowHandles();
		if (driver.getWindowHandles().size() >= 1) {

			for (String handler : handlers) {

				driver.switchTo().window(handler);

			}
			System.out.println(driver.getTitle());
		}
	}

	public String ElementExist(WebDriver driver, String Xpath, String path, int icount, int sheetname,
			String Porprtyname) throws EncryptedDocumentException, InvalidFormatException, IOException {
		String computername = InetAddress.getLocalHost().getHostName();
		String vValueFound = null;
		String[] ColumnTitle = { "Results" };
		String[] ColumnTitle1 = { "Used_By" };
		String[] completed = { "Completed" };
		String[] CellValue1 = { "Used By" + computername };
		String Norecord = "true";
		List<WebElement> text = driver.findElements(By.xpath(Xpath));
		int noElements = text.size();
		if (noElements == 0) {
			// vValueFound=driver.findElement(By.xpath(Xpath)).getText();
			String[] CellValue = { Porprtyname + "Element or Object Properties not found" };
			WriteData(ColumnTitle, icount, sheetname, CellValue, path);
			WriteData(ColumnTitle1, icount, sheetname, completed, path);
			Norecord = "false";
			return Norecord;
			// captureScreenshots(driver, "Error Captured", mainpath,subpath);
			// driver.quit();
		}
		return Norecord;
	}

	public void migrationtype(WebDriver driver, String sDefaultPath, String PPD, String Migrationtyp, int sheetnum)
			throws SAXException, IOException, ParserConfigurationException, InterruptedException {
		Set<String> handlers = driver.getWindowHandles();
		if (driver.getWindowHandles().size() >= 1) {
			for (String handler : handlers) {
				driver.switchTo().window(handler);
				System.out.println(driver.getTitle());
				if (driver.getWindowHandles().size() > 1) {
					driver.findElement(
							By.xpath("//tr/td[2]/select[@id='reason']/option[text()='Updated Email Address']")).click();
					utils.ClickObject(driver, "confirmemailadd", sDefaultPath + "\\Repository\\" + PPD + ".xml");
					// driver.findElement(By.xpath("//div[@class='content-container'/input[@id='btnConfirmContact']")).click();
				}
				break;

			}
		}

		// utils.dropdown(driver,"//html/body/div/div/div[@class='content-container']/div/div/div/table/tbody/tr[5]/td[2]/select/option[contains(text(),'Updated
		// Email Address')])","Updated Email Address");
		Thread.sleep(1000);

		driver.switchTo().parentFrame().switchTo().parentFrame().switchTo().parentFrame();
		// driver.switchTo().parentFrame();
		driver.switchTo().frame("topFrame").switchTo().frame("ifraTopMenu");
		utils.ClickObject(driver, "clickcustomercare", sDefaultPath + "\\Repository\\" + PPD + ".xml");
		utils.ClickObject(driver, "clickcustomercare", sDefaultPath + "\\Repository\\" + PPD + ".xml");
		Thread.sleep(500);
		driver.switchTo().parentFrame().switchTo().parentFrame();
		// driver.switchTo().frame("mainFrame");
		driver.switchTo().frame("leftFrame");
		utils.ClickObject(driver, "Migrations", sDefaultPath + "\\Repository\\" + PPD + ".xml");
		// utils.ClickObject(driver, "Migrations",
		// sDefaultPath+"\\Repository\\"+PPD+".xml");
		utils.ClickObject(driver, Migrationtyp, sDefaultPath + "\\Repository\\" + PPD + ".xml");
		// utils.ClickObject(driver, "MigrPosttoPost",
		// sDefaultPath+"\\Repository\\"+PPD+".xml");
		// *[@id='pptpPost2pptpPost_WEB']/font
		Thread.sleep(2000);
		Alert alert = driver.switchTo().alert();
		alert.accept();
	}

	public void creditoverpageMig(WebDriver driver) throws InterruptedException {
		driver.switchTo().parentFrame();
		driver.switchTo().frame("mainFrame");
		driver.switchTo().frame("rightCFrame");
		driver.findElement(By.xpath("//*[@id='maxIcon']")).click();
		driver.switchTo().frame("tabContenIfra");
		driver.switchTo().frame(driver.findElement(By.xpath(
				"//iframe[contains(@src,'/custcare/custsvc/basebusiness/hybridToPostpaidAtion.do?method=init&rectype=CustPostIntraMig&subRecType=PostIntraMig&targetPayType=pptpPost&oldPayType=pptpPost&ClearSession=true&sourceURL')]")));
		// Accepting alert
		driver.findElement(By.xpath("//*[@id='creditVetting']")).click();
		JavascriptExecutor js = (JavascriptExecutor) driver;
		js.executeScript("window.scrollBy(0,1000)");
		driver.findElement(By.xpath("//*[@id='next']")).click();
		Thread.sleep(2000);
	}

	public String prodseletion(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet, Recordset record,
			ResultSet resultset, String Type, String sDefaultPath, String PPD, String location, String DataPath,
			int sheetnum) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] NOCustomer = { "NoCustomer" };
		driver.findElement(By.xpath("//select[contains(@id,'prodTD_')]")).click();

		driver.switchTo().parentFrame().switchTo().parentFrame().switchTo().parentFrame().switchTo().parentFrame();
		windowhandler(driver);
		Thread.sleep(5000);
		driver.switchTo().frame("topFrame");
		// driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'actionType=commonTree&recType=CustPostIntraMig&catalogType=ROOT&productType=ProdType_Person&custType=PersonCustomer&recChannel=bsacHal&solutionID=&product.*')]")));
		String productID = getCellData("Main_Product_ID", iCount, sheet, record, resultset, Type);

		List<WebElement> ListProdID = driver.findElements(By.xpath("//div/input[@name='funcID']")); // Note
																									// pass
																									// the
																									// xpath
																									// of
																									// the
																									// dropdown
																									// you
																									// want
																									// to
																									// work
																									// on
		for (int i1 = 1; i1 <= ListProdID.size(); i1++) {
			outputValue = ListProdID.get(i1).getAttribute("id");
			if (outputValue.matches(productID)) {
				Thread.sleep(2000);
				driver.findElement(By.xpath("//div/input[@name='funcID'][@id='" + productID + "']")).click();
				// ListProdID.get(i1).click();
				Thread.sleep(1000);
				driver.findElement(By
						.xpath("//form[@name='selectProductForm']/table[2]/tbody/tr/td/input[@type='button'][@value='OK']"))
						.click();
				alert(driver, DataPath, iCount, sheetnum);
				break;

			}
		}

		return outputValue;
	}

	public String VasandPricepalnsel(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet, Recordset record,
			ResultSet resultset, String Type, String sDefaultPath, String PPD, String location, String DataPath,
			int sheetnum) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] NOCustomer = { "NoCustomer" };
		String Norecord = null;
		windowhandler(driver);
		String PriceplanidID = getCellData("PricePlanID", iCount, sheet, record, resultset, Type);
		String productID = getCellData("Main_Product_ID", iCount, sheet, record, resultset, Type);
		driver.switchTo().parentFrame();
		// driver.switchTo().parentFrame();
		// driver.switchTo().parentFrame();
		driver.switchTo().frame("mainFrame");
		driver.switchTo().frame("rightCFrame");
		driver.switchTo().frame("tabContenIfra");
		String productName = getCellData("Product_Name", iCount, sheet, record, resultset, Type);

		driver.switchTo().frame(driver.findElement(By.xpath(
				"//iframe[contains(@src,'/custcare/custsvc/basebusiness/hybridToPostpaidAtion.do?method=init&rectype=CustPostIntraMig&subRecType=PostIntraMig&targetPayType=pptpPost&oldPayType=pptpPost&ClearSession=true&sourceURL')]")));
		driver.findElement(By.xpath("//*[@value='Select VAS']")).click();
		driver.switchTo().parentFrame().switchTo().parentFrame().switchTo().parentFrame().switchTo().parentFrame();
		windowhandler(driver);
		// driver.switchTo().parentFrame();
		Thread.sleep(8000);
		driver.switchTo().frame("commonFrame");

		// driver.findElement(By.xpath("//iframe[contains(@src,'/custcare/product/selectProductAction.do?actionType=selectProduct&cmd=userBusiness&isHasCautioner=&isSimple=&curProcductID=.*')]")));
		// driver.findElement(By.xpath("//table[@class='list_table']/tbody/tr[2]/td[1]/span[contains(text()='Pinnacle
		// Postpaid 2018']")).click();
		// driver.findElement(By.xpath("//span[contains[text(),'"+productName+"']")).click();
		driver.findElement(By.xpath("//img[@id='" + productID + "']")).click();

		// driver.findElement(By.xpath("//iframe[contains(text()='Pinnacle
		// 500MB[149]')]")).click();
		// driver.findElement(By.xpath("//iframe[contains(text()='Pinnacle
		// 500MB[149]')]")).click();

		List<WebElement> ListPricepalid = driver
				.findElements(By.xpath("//table[@class='list_table'][@id='tabMain']/tbody[1]/tr"));
		for (int i2 = 3; i2 < ListPricepalid.size(); i2++) {
			ListPricepalid1 = driver
					.findElement(By
							.xpath("//table[@class='list_table'][@id='tabMain']/tbody[1]/tr[" + i2 + "]/td[1]/span[2]"))
					.getAttribute("title");
			// ListPricepalid1.get(index)
			// .getAttribute("title");
			PriceplanidID = PriceplanidID.trim();

			// driver.findElement(By.xpath("//tr/td/input[@id='radio0001.0103']")).click();
			ListPricepalid1 = ListPricepalid1.trim();
			if (ListPricepalid1.matches(PriceplanidID)) {
				String strradioidid = driver
						.findElement(By.xpath(
								"//table[@class='list_table'][@id='tabMain']/tbody[1]/tr[" + i2 + "]/td[1]/input"))
						.getAttribute("id");
				String strradioidname = driver
						.findElement(By.xpath(
								"//table[@class='list_table'][@id='tabMain']/tbody[1]/tr[" + i2 + "]/td[1]/input"))
						.getAttribute("name");
				String strradioidvalue = driver
						.findElement(By.xpath(
								"//table[@class='list_table'][@id='tabMain']/tbody[1]/tr[" + i2 + "]/td[1]/input"))
						.getAttribute("value");
				driver.findElement(By.xpath("//tbody[1]/tr[" + i2 + "]/td[1]/input[@id='" + strradioidid + "']"))
						.click();
				driver.switchTo().parentFrame();
				driver.findElement(By.xpath("//input[@id='saveBt']")).click();
				alert(driver, DataPath, iCount, sheetnum);
				break;

			}
			Thread.sleep(1000);
		}

		return ListPricepalid1;
	}

	public String QuickDeals(WebDriver driver,String strProductType, String Dealtype, String productitem, String PricePlanId, String months,
			String Dealcode, String Imei, String sDefaultPath, String PPD, int iCount, int sheetnum, String DataPath)
			throws InterruptedException, InvalidFormatException, IOException, SAXException,
			ParserConfigurationException {
		String Dealtype1 = Dealtype.toLowerCase();
		String[] sColumn = { "Results" };
		String[] sColumn1 = { "Used_By" };
		String[] sData1 = { "Completed" };
		String[] Proderr = { "Prodcut Mentioned Excel does not Exists" };
		String[] Priceplanerr = { "Priceplan Mentioned Excel does not Exists" };
		String[] Monthsd = { "Months Mentioned Excel does not Exists" };
		String[] Dealerr = { "Deal Code Metioned Does Not Exists" };
		String AlertText, alertmessage = null;
		String Norecord = "true";
		switch (Dealtype1) {
		case "sim only":
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			// Actions action = new Actions(driver);
			// action.moveToElement(driver.findElement(By.xpath("//input[@id='RADIO_controlProductDiv5']"))).doubleClick().build().perform();
			// driver.findElement(By.xpath("//span[@id='span_simonly']/input[@id='RADIO_controlProductDiv5'][@value='5']")).click();
			driver.findElement(By.xpath("//input[@id='RADIO_controlProductDiv5']")).click();
			Thread.sleep(2000);
			// String productitem ="MPWHLSLPP0A";
			driver.findElement(
					By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='"+strProductType+"']"))
					.click();
			Thread.sleep(400);
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> productlist = driver
					.findElements(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option"));
			for (int z = 1; z <= productlist.size(); z++) {
				int prdtsize = productlist.size();

				String productid = driver
						.findElement(By
								.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				if (productitem.equalsIgnoreCase(productid)) {
					driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option["
							+ z + "][@value='" + productid + "']")).click();
					break;
				}
				if (z == prdtsize) {

					WriteData(sColumn, iCount, sheetnum, Proderr, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}
			// String months ="24";
			// driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> monthssize = driver
					.findElements(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[3]/td[3]/select/option"));
			int monthssiz = monthssize.size();
			for (int z = 1; z <= monthssize.size(); z++) {

				String monthsname = driver
						.findElement(By
								.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[3]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				//int monthsnamee = Integer.parseInt(monthsname);
				if (monthsname.contains(months)) {
					driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[3]/td[3]/select/option["
							+ z + "][@value='" + monthsname + "']")).click();
					break;
				}
				if (z == monthssiz) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Monthsd, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}
			// String PricePlanId ="PPWHLSLTPA0A";
			// driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> PricePlanlist = driver
					.findElements(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[4]/td[3]/select/option"));
			int pricesize = PricePlanlist.size();
			for (int z = 1; z <= PricePlanlist.size(); z++) {

				String Prceplanid = driver
						.findElement(By
								.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[4]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				if (PricePlanId.equalsIgnoreCase(Prceplanid)) {
					driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[4]/td[3]/select/option["
							+ z + "][@value='" + PricePlanId + "']")).click();
					break;
				}
				if (z == pricesize) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Priceplanerr, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}
			break;
		case "by priceplan with hero deal":
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			driver.findElement(By.xpath("//input[@id='RADIO_controlProductDiv2']")).click();
			Thread.sleep(1000);
			// driver.findElement(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// Thread.sleep(1000);
			List<WebElement> productlist1 = driver
					.findElements(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr/td[3]/select/option"));
			for (int z = 1; z <= productlist1.size(); z++) {
				int prdtsize = productlist1.size();

				String productid = driver
						.findElement(By
								.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				// String productitem="MPPNBLKPP";
				if (productitem.equalsIgnoreCase(productid)) {
					driver.findElement(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr/td[3]/select/option[" + z
							+ "][@value='" + productid + "']")).click();
					break;
				}
				if (z == prdtsize) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Proderr, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}

			// driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> monthssize1 = driver
					.findElements(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[2]/td[3]/select/option"));
			monthssiz = monthssize1.size();
			for (int z = 1; z <= monthssize1.size(); z++) {

				String monthsname = driver
						.findElement(By.xpath(
								"//div[@id='DIV_priceplan']/div/table/tbody/tr[2]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				//int monthsnamee = Integer.parseInt(monthsname);
				if (monthsname.contains(months)) {
					driver.findElement(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[2]/td[3]/select/option["
							+ z + "][@value='" + monthsname + "']")).click();
					break;
				}
				if (z == monthssiz) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Monthsd, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}
			Thread.sleep(500);
			// PricePlanId ="PPPNBLK500MPP";
			// driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> PricePlanlist1 = driver
					.findElements(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[3]/td[3]/select/option"));
			pricesize = PricePlanlist1.size();
			for (int z = 1; z <= PricePlanlist1.size(); z++) {

				String Prceplanid = driver
						.findElement(By.xpath(
								"//div[@id='DIV_priceplan']/div/table/tbody/tr[3]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				if (PricePlanId.equalsIgnoreCase(Prceplanid)) {
					driver.findElement(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[3]/td[3]/select/option["
							+ z + "][@value='" + PricePlanId + "']")).click();
					break;
				}
				if (z == pricesize) {
					WriteData(sColumn, iCount, sheetnum, Priceplanerr, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}
			Thread.sleep(500);
			List<WebElement> Deallist = driver
					.findElements(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[4]/td[3]/select/option"));
			int dealsize = Deallist.size();
			if (dealsize == 0) {

			} else {
				for (int z = 1; z <= dealsize; z++) {

					String Dealcodee = driver
							.findElement(By.xpath(
									"//div[@id='DIV_priceplan']/div/table/tbody/tr[4]/td[3]/select/option[" + z + "]"))
							.getAttribute("value");
					if (Dealcode.equalsIgnoreCase(Dealcodee)) {
						driver.findElement(
								By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[4]/td[3]/select/option[" + z
										+ "][@value='" + PricePlanId + "']"))
								.click();
						Thread.sleep(500);

						break;
					}
					if (z == pricesize) {
						WriteData(sColumn, iCount, sheetnum, Dealerr, DataPath);
						WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
						Norecord = "false";
						return Norecord;
					}
				}
			}
			driver.findElement(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[5]/td[3]/input")).sendKeys(Imei);
			driver.findElement(By.xpath("//div[@id='DIV_priceplan']/div/table/tbody/tr[6]")).click();
			// String Imeierror=
			// utils.ElementRepXpath(driver,"ImeiErr",sDefaultPath+"\\Repository\\"+PPD+".xml");
			// String Imeierror1=
			// "//div[@id='DIV_priceplan']/div/table/tbody/tr[5]/td[4]/font[@id='imei.msg2']";
			Thread.sleep(300);
			if (utils.checkIfObjectIsDisplayed(driver, "ImeiErr", sDefaultPath + "\\Repository\\" + PPD + ".xml")) {
				String[] imeerr = { "Imei is not valid" };
				WriteData(sColumn, iCount, sheetnum, imeerr, DataPath);
				WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
				Norecord = "false";
				return Norecord;

			}
			break;

		case "by priceplan with fixed deal":

			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			// Actions action = new Actions(driver);
			// action.moveToElement(driver.findElement(By.xpath("//input[@id='RADIO_controlProductDiv5']"))).doubleClick().build().perform();
			// driver.findElement(By.xpath("//span[@id='span_simonly']/input[@id='RADIO_controlProductDiv5'][@value='5']")).click();
			driver.findElement(By.xpath("//input[@id='RADIO_controlProductDiv6']")).click();
			Thread.sleep(1000);

			// driver.findElement(By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// Thread.sleep(1000);
			List<WebElement> productlist2 = driver.findElements(
					By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr/td[3]/select/option"));
			for (int z = 1; z <= productlist2.size(); z++) {
				int prdtsize = productlist2.size();

				String productid = driver.findElement(By.xpath(
						"//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				// String productitem="MPPNBLKPP";
				if (productitem.equalsIgnoreCase(productid)) {
					driver.findElement(
							By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr/td[3]/select/option[" + z
									+ "][@value='" + productid + "']"))
							.click();
					break;
				}
				if (z == prdtsize) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Proderr, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;
				}
			}
			// String months ="24";
			// driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> monthssize2 = driver.findElements(
					By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[2]/td[3]/select/option"));
			monthssiz = monthssize2.size();
			for (int z = 1; z <= monthssize2.size(); z++) {

				String monthsname = driver.findElement(By.xpath(
						"//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[2]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				//int monthsnamee = Integer.parseInt(monthsname);
				if (monthsname.contains(months)) {
					driver.findElement(
							By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[2]/td[3]/select/option["
									+ z + "][@value='" + monthsname + "']"))
							.click();
					break;
				}
				if (z == monthssiz) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Monthsd, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;

				}
			}
			Thread.sleep(500);
			// String PricePlanId ="PPPNBLK500MPP";
			// driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']")).click();
			// String
			// sfgd=driver.findElement(By.xpath("//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"+productitem+"']")).getText();
			List<WebElement> PricePlanlist2 = driver.findElements(
					By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[3]/td[3]/select/option"));
			pricesize = PricePlanlist2.size();
			for (int z = 1; z <= PricePlanlist2.size(); z++) {

				String Prceplanid = driver.findElement(By.xpath(
						"//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[3]/td[3]/select/option[" + z + "]"))
						.getAttribute("value");
				if (PricePlanId.equalsIgnoreCase(Prceplanid)) {
					driver.findElement(
							By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[3]/td[3]/select/option["
									+ z + "][@value='" + PricePlanId + "']"))
							.click();
					break;
				}
				if (z == pricesize) {
					System.out.println("Productid not Found");
					WriteData(sColumn, iCount, sheetnum, Priceplanerr, DataPath);
					WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
					Norecord = "false";
					return Norecord;

				}
			}
			Thread.sleep(500);

			Thread.sleep(500);
			List<WebElement> Deallist1 = driver.findElements(
					By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[4]/td[3]/select/option"));
			dealsize = Deallist1.size();
			if (dealsize == 0) {

			} else {
				for (int z = 1; z <= dealsize; z++) {

					String Dealcodee = driver.findElement(
							By.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[7]/td[3]/select/option["
									+ z + "]"))
							.getAttribute("value");
					if (Dealcode.equalsIgnoreCase(Dealcodee)) {
						driver.findElement(By
								.xpath("//div[@id='DIV_priceplan4FixedDeal']/div/table/tbody/tr[7]/td[3]/select/option["
										+ z + "][@value='" + PricePlanId + "']"))
								.click();
						Thread.sleep(500);

						break;
					}
					if (z == pricesize) {
						System.out.println("Productid not Found");
						WriteData(sColumn, iCount, sheetnum, Dealerr, DataPath);
						WriteData(sColumn1, iCount, sheetnum, sData1, DataPath);
						Norecord = "false";
						return Norecord;

					}
				}
			}

			break;

		case "custom deal":

			break;

		}
		return Norecord;
	}

	public void BirthDateFunctions(WebDriver driver, String BirthDates) {
		try {

			long wait = 1000;
			String date = BirthDates;
			String splitter[] = date.split("-");
			String Month_Date = splitter[1], DayDate = splitter[0], Year_Date = splitter[2];
			driver.switchTo().frame(driver.findElement(
					By.xpath("//iframe[contains(@src,'/custcare/js/newcalendar/DatePicker/My97DatePicker.htm')]")));

			driver.findElement(By.xpath("//input[@class='yminput'][@tabindex='2']")).click();
			driver.findElement(By.xpath("//input[@class='yminput'][@tabindex='2']")).sendKeys();
			// driver.findElement(By.xpath("//input[@class='yminput'][@tabindex='2']")).clear();
			driver.findElement(By.xpath("//input[@class='yminput'][@tabindex='2']")).sendKeys(Year_Date);
			driver.findElement(By.xpath("//input[@class='yminput'][@tabindex='1']")).click();
			driver.findElement(By.xpath("//input[@class='yminput'][@tabindex='1']")).sendKeys(Month_Date);

			List<WebElement> list_Alldays = driver.findElements(By.xpath("//*[@id='_z_0-mid']/tbody/tr/td"));
			for (int iday = 0; iday < list_Alldays.size(); iday++) {
				String Days = list_Alldays.get(iday).getText();
				if (Days.matches(DayDate)) {
					list_Alldays.get(iday).click();
					Thread.sleep(wait);
					break;
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}

	}

	public Recordset ConnectFillo(String path, String Query) throws FilloException {

		Fillo fillo = new Fillo();

		Recordset record;
		// List<String> data = new ArrayList<String>();

		String value = null;

		connect = fillo.getConnection(path);
		record = connect.executeQuery(Query);

		/*
		 * while(record.next()) { String valContain=record.getField("Used_By");
		 * 
		 * if (valContain.isEmpty()) { value=record.getField(vColumnValue);
		 * break; } }
		 */

		return record;

	}

	public void Popup(WebDriver driver) {
		Set<String> handlers = driver.getWindowHandles();
		if (driver.getWindowHandles().size() >= 1) {
			for (String handler : handlers) {
				driver.switchTo().window(handler);
				if (driver.getCurrentUrl().contains("VASProductTree")) {
					System.out.println("Get focus on Popup window");
					break;
				}
			}
		}
	}

	public void WriteData(String[] sColumn, int Row, int shet, String[] sData, String filepath)
			throws IOException, InvalidFormatException {
		int CoulmnNo = 0;
		FileInputStream file = new FileInputStream(filepath);
		org.apache.poi.ss.usermodel.Workbook wb = WorkbookFactory.create(file);
		org.apache.poi.ss.usermodel.Sheet sheet = wb.getSheetAt(shet);

		org.apache.poi.ss.usermodel.Cell cell = null;
		Row row = sheet.getRow(0);

		for (int c = 0; c < sColumn.length; c++) {
			for (int i = 0; i < row.getLastCellNum(); i++) {
				if (row.getCell(i).getStringCellValue().trim().equals(sColumn[c])) {
					CoulmnNo = i;
					Row raw = sheet.getRow(Row);
					cell = raw.createCell(CoulmnNo);
					cell.setCellValue((String) sData[c]);
					break;
				}
			}
		}
		FileOutputStream fileOut = new FileOutputStream(filepath);
		wb.write(fileOut);
		fileOut.close();
	}

	public void alert(WebDriver driver, String location, int iCount, int sheetnum)
			throws IOException, InvalidFormatException {
		WebDriverWait wait = new WebDriverWait(driver, 2);
		// Thread.sleep(2000);
		wait.until(ExpectedConditions.alertIsPresent());
		// Alert alert = ExpectedConditions.alertIsPresent()
		Alert alert = driver.switchTo().alert();
		// alert.
		String stext = alert.getText();
		String[] siderrtext = { stext };
		alert.accept();
		String[] sResults = { "Results" };
		// String[] sUsed = { "Used" };
		// DataFun.WriteData(sColumn, Row, sData, filepath);
		// sLocation=sLocation+"PPDExcel.xls";
		WriteData(sResults, iCount, sheetnum, siderrtext, location);

	}

	public String getCellData(String strColumn, int iRow, Sheet sheet, Recordset record, ResultSet resultset,
			String Type) throws Exception {
		String sValue = null;
		switch (Type.toUpperCase()) {

		case "EXCEL":

			Row row = sheet.getRow(0);
			for (int i = 0; i < columnCount(sheet); i++) {
				if (row.getCell(i).getStringCellValue().trim().equals(strColumn)) {
					Row raw = sheet.getRow(iRow);
					Cell cell = raw.getCell(i);
					DataFormatter formatter = new DataFormatter();
					sValue = formatter.formatCellValue(cell);
					break;
				}

			}
			break;

		case "FILLO":

			if (iRow == fillonum) {
				if (record.next()) {
					fillonum = iRow + 1;
					sValue = record.getField(strColumn);
				}
			} else {
				sValue = record.getField(strColumn);
			}
			break;

		case "SQLSERVER":

			if (iRow == fillonum) {
				if (resultset.next()) {
					fillonum = iRow + 1;
					sValue = resultset.getString(strColumn);
				}
			} else {
				sValue = resultset.getString(strColumn);
			}
			break;

		}

		return sValue;

	}

	public void PageFocus(WebDriver driver) throws Exception {
		Set<String> handlers = driver.getWindowHandles();
		if (driver.getWindowHandles().size() >= 1) {
			for (String handler : handlers) {
				driver.switchTo().window(handler);
				if (driver.getCurrentUrl().contains("Popup")) {
					
					System.out.println("Get focus on Popup window");
					break;
				}
			}
		}
		// Thread.sleep(2000);
	}

	public void BirtDayFunctions(WebDriver driver, String BirthDates, String DateType) // dd/mm/yyyy
	{
		try {
			long wait = 1000;
			String date = BirthDates, Year_Date = "", MonthFormat = "";
			String splitter[] = date.split("/");
			int Month_Date = 0, DayDate = 0;
			if (DateType.matches("BirthDate")) {
				Month_Date = Integer.parseInt(splitter[1]);
				DayDate = Integer.parseInt(splitter[0]);
				Year_Date = splitter[2];
			} else if (DateType.matches("ExpiryDate")) {
				Month_Date = Integer.parseInt(splitter[1]);
				DayDate = Integer.parseInt(splitter[0]);
				Year_Date = splitter[2];
			} else if (DateType.matches("MonthYearDate")) {
				Month_Date = Integer.parseInt(splitter[1]);
				DayDate = Integer.parseInt(splitter[0]);
			}
			String[] AMonths = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
			for (int we = 0; we < AMonths.length; we++) {
				if (Month_Date == 1) {
					we = we + 1;
				}
				if (Month_Date == (we)) {
					MonthFormat = AMonths[we];
					break;
				}
			}
			switch (DateType) {

			case "BirthDate":
				driver.switchTo().frame(driver.findElement(
						By.xpath("//div/iframe[@src='/custcare/js/newcalendar/DatePicker/My97DatePicker.htm']")));
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).sendKeys(Year_Date);
				/** Cater for Months */
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).sendKeys(MonthFormat);
				/** Cater for Days */
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				driver.switchTo().parentFrame();
				break;
			case "ExpiryDate":
				driver.switchTo().frame(driver.findElement(
						By.xpath("//div/iframe[@src='/custcare/js/newcalendar/DatePicker/My97DatePicker.htm']")));
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).sendKeys(Year_Date);
				/** Cater for Months */
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).sendKeys(MonthFormat);
				/** Cater for Days */
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				driver.switchTo().parentFrame();
				break;
			case "MonthYearDate":
				driver.switchTo().frame(driver.findElement(
						By.xpath("//div/iframe[@src='/custcare/js/newcalendar/DatePicker/My97DatePicker.htm']")));
				List<WebElement> list_MYears = driver
						.findElements(By.xpath("//html/body/div[1]/div[2]/table/tbody/tr/td"));
				outerloop:
				/** Cater for years */
				for (int my = 0; my < list_MYears.size(); my++) {
					String Years = list_MYears.get(my).getText();
					String Emp = " " + BirthDates;
					if (Years.matches(Emp)) {
						list_MYears.get(my).click();
						Thread.sleep(wait);
						driver.switchTo().parentFrame();
						break;
					}
				}
			default:
				break;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}
	public void ChannelBirtDayFunctions(WebDriver driver, String BirthDates, String DateType) // dd/mm/yyyy
	{
		try {
			long wait = 1000;
			String date = BirthDates, Year_Date = "", MonthFormat = "";
			String splitter[] = date.split("/");
			int Month_Date = 0, DayDate = 0;
			if (DateType.matches("BirthDate")) {
				Month_Date = Integer.parseInt(splitter[1]);
				DayDate = Integer.parseInt(splitter[0]);
				Year_Date = splitter[2];
			} else if (DateType.matches("ExpiryDate")) {
				Month_Date = Integer.parseInt(splitter[1]);
				DayDate = Integer.parseInt(splitter[0]);
				Year_Date = splitter[2];
			} else if (DateType.matches("MonthYearDate")) {
				Month_Date = Integer.parseInt(splitter[1]);
				DayDate = Integer.parseInt(splitter[0]);
			}
			String[] AMonths = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
			for (int we = 0; we < AMonths.length; we++) {
				if (Month_Date == 1) {
					we = we + 1;
				}
				if (Month_Date == (we)) {
					MonthFormat = AMonths[we];
					break;
				}
			}
			switch (DateType) {

			case "BirthDate":
				driver.switchTo().frame(driver.findElement(
						By.xpath("//div/iframe[@src='/channel/js/DatePicker/My97DatePicker.htm']")));
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).sendKeys(Year_Date);
				/** Cater for Months */
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).sendKeys(MonthFormat);
				/** Cater for Days */
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				driver.switchTo().parentFrame();
				break;
			case "ExpiryDate":
				driver.switchTo().frame(driver.findElement(
						By.xpath("//div/iframe[@src='/channel/js/DatePicker/My97DatePicker.htm']")));
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[4]/input")).sendKeys(Year_Date);
				/** Cater for Months */
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).click();
				driver.findElement(By.xpath("//*[@id='dpTitle']/div[3]/input")).sendKeys(MonthFormat);
				/** Cater for Days */
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//html/body/div[1]/div[3]/table/tbody/tr/td[text()='" + DayDate + "']"))
						.click();
				driver.switchTo().parentFrame();
				break;
			case "MonthYearDate":
				driver.switchTo().frame(driver.findElement(
						By.xpath("//div/iframe[@src='/channel/js/DatePicker/My97DatePicker.htm']")));
				List<WebElement> list_MYears = driver
						.findElements(By.xpath("//html/body/div[1]/div[2]/table/tbody/tr/td"));
				outerloop:
				/** Cater for years */
				for (int my = 0; my < list_MYears.size(); my++) {
					String Years = list_MYears.get(my).getText();
					String Emp = " " + BirthDates;
					if (Years.matches(Emp)) {
						list_MYears.get(my).click();
						Thread.sleep(wait);
						driver.switchTo().parentFrame();
						break;
					}
				}
			default:
				break;
			}

		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
	}

	public void Organisation(WebDriver driver, String OrganisationID) {
		try {

			PageFocus(driver);
			/** Organisation Tree */
			// Thread.sleep(700);
			driver.findElement(By.xpath("//h2[@id='queryTab']/a/div[text()='Search']")).click();
			driver.findElement(By.xpath("//h2[@id='queryTab']/a/div[text()='Search']")).click();
			Thread.sleep(700);
			driver.findElement(By.name("queryId")).sendKeys(OrganisationID);
			driver.findElement(By.name("Submit")).click();
			Thread.sleep(1000);
			driver.switchTo().frame("listFrame");
			Thread.sleep(1000);
			if ((driver.findElement(By.xpath("//tr[2]/td")).getText()).matches(OrganisationID)) {
				driver.findElement(By.xpath("//tr[2]/td")).click();
			} else {
				System.out.println(driver.findElement(By.xpath("//tr[2]/td")).getText());
				Set<String> handlers = driver.getWindowHandles();
				if (driver.getWindowHandles().size() >= 1) {
					for (String handler : handlers) {
						driver.switchTo().window(handler).close();
						;
					}
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void OrganisationDeals(WebDriver driver, String OrganisationID) {
		try {

			PageFocus(driver);
			/** Organisation Tree */
			// Thread.sleep(700);
			driver.findElement(By.xpath("//h2[@id='queryTab']/a/div[text()='Search']")).click();
			driver.findElement(By.xpath("//h2[@id='queryTab']/a/div[text()='Search']")).click();
			Thread.sleep(700);
			driver.findElement(By.name("queryId")).sendKeys(OrganisationID);
			driver.findElement(By.name("Submit")).click();
			Thread.sleep(1000);
			driver.switchTo().frame("listFrame");
			Thread.sleep(1000);
			if ((driver.findElement(By.xpath("//tr[2]/td")).getText()).matches(OrganisationID)) {
				driver.findElement(By.xpath("//tr[2]/td")).click();
			 
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public String Vas(WebDriver driver, String Discount, String Emailvas, String Product, String Addvas,
			String Coonectfee, int iCount, int sheetnum, String DataPath) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] NOCustomer = { "NoCustomer" };
		String[] Usedby = { "Used_By" };
		String alrtiter = "true";
		String Norecord = "true";
		String EleExist = "true";
		String connet[] = { "Please Enter Connection or Installation Fee for Connect Product In Excel" };
		String Poduc[] = { "Product Information is Null In Excel" };
		// driver.switchTo().frame("mainFrame");
		driver.findElement(By.xpath("//*[@value='Select VAS Product']")).click();
		driver.switchTo().parentFrame();
		Thread.sleep(2000);
		PageFocus(driver);
		if (!Product.equals("") && null != Product) {
			if (Product.contains("Fibre")) {
				boolean strmediplay = Product.contains("MediaPlay");
				String strcfibre = Product.substring(Product.length() - 7);

				if (strmediplay || strcfibre.equalsIgnoreCase("C-Fibre")) {
					// PageFocus(driver);
					// driver.findElement(By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]")).click();
					driver.findElement(
							By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]"))
							.click();
					Thread.sleep(500);
					driver.findElement(By.xpath("//span[@title='VPPPEMAILADDR']/a")).click();
					driver.findElement(By.xpath("//span[@title='VPPPEMAILADDR']/a")).click();
					// data.PageFocus(driver);
					Thread.sleep(300);
					PageFocus(driver);
					driver.switchTo().frame("topFrame");
					driver.findElement(By.xpath("//input[@name='EmailAddr']")).sendKeys(Emailvas);
					driver.findElement(By.xpath("//input[@value='Save']")).click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Thread.sleep(800);
						PageFocus(driver);
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}
					Thread.sleep(300);
					driver.switchTo().parentFrame();
					PageFocus(driver);
					driver.switchTo().frame("topFrame");
					driver.findElement(By.xpath("//input[@value='Close']")).click();
					PageFocus(driver);
					driver.switchTo().parentFrame();
					Thread.sleep(1000);
					driver.findElement(
							By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]"))
							.click();

				}
				if (strcfibre.equalsIgnoreCase("Connect")) {
					// PageFocus(driver);
					driver.findElement(By
							.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'C-Fibre Fee')]"))
							.click();
					if (!Coonectfee.equals("") && null != Coonectfee) {
						String[] strconnectfee = Coonectfee.split(";");
						int strconnectfeesize = strconnectfee.length;
						for (int f = 0; f < strconnectfeesize; f++) {
							EleExist = ElementExist(driver, "//span[contains(text(),'" + strconnectfee[f] + "')]",
									DataPath, iCount, sheetnum, strconnectfee[f]);
							if (EleExist.equals("false")) {
								Thread.sleep(800);
								driver.findElement(By.xpath("//input[@value='Cancel']")).click();
								Norecord = "false";
								return Norecord;
							}
							String strtitle = driver
									.findElement(By.xpath("//span[contains(text(),'" + strconnectfee[f] + "')]"))
									.getAttribute("title");
							driver.findElement(By.xpath("//*[@value='" + strtitle + "']")).click();
							alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
							if (alrtiter.equals("false")) {
								Thread.sleep(800);
								driver.findElement(By.xpath("//input[@value='Cancel']")).click();
								Norecord = "false";
								return Norecord;
							}
						}
					} else {
						WriteData(Results, iCount, sheetnum, connet, DataPath);
						WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
						Norecord = "false";
						driver.findElement(By.xpath("//*[@value='Save']")).click();
						return Norecord;
					}
				}
				if (!Discount.equals("") && null != Discount) {
					// driver.switchTo().parentFrame();

					// driver.findElement(By.xpath("//table/tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'C-Fibre
					// Discount')]")).click();

					driver.findElement(By
							.xpath("//table/tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'C-Fibre Discount')]"))
							.click();
					Thread.sleep(300);
					EleExist = ElementExist(driver, "//*[@value='PPFTTHMPDS" + Discount + "']", DataPath, iCount,
							sheetnum, "Discount metioned in Excel");
					if (EleExist.equals("false")) {
						Thread.sleep(800);
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}
					driver.findElement(By.xpath("//*[@value='PPFTTHMPDS" + Discount + "']")).click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Thread.sleep(800);
						PageFocus(driver);
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}
				}
			}
			if (!Addvas.equals("") && null!= Addvas) {
				// driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(
						By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]"))
						.click();
				Thread.sleep(500);
				String[] strvas = Addvas.split(";");
				int vassize = strvas.length;

				for (int f = 0; f < vassize; f++) {
					EleExist = ElementExist(driver, "//span[contains(text(),'" + strvas[f] + "')]", DataPath, iCount,
							sheetnum, strvas[f]);
					if (EleExist.equals("false")) {
						Thread.sleep(800);
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}
					String strtitle = driver.findElement(By.xpath("//span[contains(text(),'" + strvas[f] + "')]"))
							.getAttribute("title");
					driver.findElement(By.xpath("//*[@value='" + strtitle + "']")).click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Thread.sleep(800);
						PageFocus(driver);
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}

				}
			}
			
		} else {
			WriteData(Results, iCount, sheetnum, Poduc, DataPath);
			WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
			Norecord = "false";
			driver.findElement(By.xpath("//*[@value='Save']")).click();
			return Norecord;

		}
		 driver.findElement(By.xpath("//*[@value='Save']")).click();
		alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
		if (alrtiter.equals("false")) {
			driver.findElement(By.xpath("//input[@value='Cancel']")).click();
			Thread.sleep(800);
			PageFocus(driver);
			driver.switchTo().parentFrame();
			driver.findElement(By.xpath("//input[@value='Cancel']")).click();
			Norecord = "false";
			return Norecord;
		}
		// driver.switchTo().parentFrame();
		// PageFocus(driver);
	
		return Norecord;
	}

	public String BCVas(WebDriver driver, String Discount, String Emailvas, String Product, String Addvas,
			String Coonectfee, int iCount, int sheetnum, String DataPath, String Priceplan) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] NOCustomer = { "NoCustomer" };
		String[] Usedby = { "Used_By" };
		String alrtiter = "true";
		String Norecord = "true";
		String EleExist = "true";
		String connet[] = { "Please Enter Connection or Installation Fee for Connect Product In Excel" };
		String Poduc[] = { "Product Information is Null In Excel" };
		// driver.switchTo().frame("mainFrame");
		driver.findElement(By.xpath("//*[@value='Select VAS Product']")).click();
		driver.switchTo().parentFrame();
		Thread.sleep(2000);
		PageFocus(driver);
		if (!Product.equals("") && null != Product) {
			String strcfibre = Product.substring(Product.length() - 7);
			if (Product.contains("Fibre")) {
				boolean strmediplay = Product.contains("MediaPlay");

				if (strmediplay || strcfibre.equalsIgnoreCase("C-Fibre")) {
					// PageFocus(driver);
					// driver.findElement(By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]")).click();
					driver.findElement(
							By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]"))
							.click();
					Thread.sleep(500);
					driver.findElement(By.xpath("//span[@title='VPPPEMAILADDR']/a")).click();
					driver.findElement(By.xpath("//span[@title='VPPPEMAILADDR']/a")).click();
					// data.PageFocus(driver);
					Thread.sleep(300);
					PageFocus(driver);
					driver.switchTo().frame("commonFrame");
					driver.findElement(By.xpath("//input[@name='EmailAddr']")).sendKeys(Emailvas);
					driver.findElement(By.xpath("//input[@value='Save']")).click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Thread.sleep(800);
						PageFocus(driver);
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}
					Thread.sleep(300);
					driver.switchTo().parentFrame();
					PageFocus(driver);
					driver.switchTo().frame("commonFrame");
					driver.findElement(By.xpath("//input[@value='Close']")).click();
					PageFocus(driver);
					driver.switchTo().parentFrame();
					Thread.sleep(1000);
					driver.findElement(
							By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]"))
							.click();

				}
			}
			if (strcfibre.equalsIgnoreCase("Connect")) {
				// PageFocus(driver);
				driver.findElement(
						By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'C-Fibre Fee')]"))
						.click();
				if (!Coonectfee.equals("") && null != Coonectfee) {
					String[] strconnectfee = Coonectfee.split(";");
					int strconnectfeesize = strconnectfee.length;
					for (int f = 0; f < strconnectfeesize; f++) {
						EleExist = ElementExist(driver, "//span[contains(text(),'" + strconnectfee[f] + "')]", DataPath,
								iCount, sheetnum, strconnectfee[f]);
						if (EleExist.equals("false")) {
							Thread.sleep(800);
							driver.findElement(By.xpath("//input[@value='Cancel']")).click();
							Norecord = "false";
							return Norecord;
						}
						String strtitle = driver
								.findElement(By.xpath("//span[contains(text(),'" + strconnectfee[f] + "')]"))
								.getAttribute("title");
						driver.findElement(By.xpath("//*[@value='" + strtitle + "']")).click();
						alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
						if (alrtiter.equals("false")) {
							Thread.sleep(800);
							driver.findElement(By.xpath("//input[@value='Cancel']")).click();
							Norecord = "false";
							return Norecord;
						}
					}
				}
			}
			if (!strcfibre.equalsIgnoreCase("Connect") && (!Priceplan.equals(""))) {
				// Product
				Thread.sleep(300);
				driver.switchTo().parentFrame();
				PageFocus(driver);
				driver.switchTo().frame("commonFrame");

				driver.findElement(By.xpath("//form[@name='selectProductForm']/fieldset/table/tbody/tr[2]/td/img"))
						.click();
				// driver.findElement(By.xpath("//form[@name='selectProductForm']/fieldset/table/tbody/tr[2]/td/img")).click();
				String strprctitid = driver.findElement(By.xpath("//span[contains(text(),'" + Priceplan + "')]"))
						.getAttribute("title");
				if (strprctitid.equals("")) {
					String[] strprcpln = { "Priceplan specified does not Exist" };
					WriteData(Results, iCount, sheetnum, strprcpln, DataPath);
					WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
					Norecord = "false";
					driver.findElement(By.xpath("//input[@value='Cancel']")).click();
					return Norecord;
				}
				String strradioid = driver.findElement(By.xpath("//tr[@productID='" + strprctitid + "']"))
						.getAttribute("id");
				driver.findElement(By.xpath("//input[@value='" + strradioid + "']")).click();
			}
			if (!Discount.equals("") && null != Discount) {
				// driver.switchTo().parentFrame();

				// driver.findElement(By.xpath("//table/tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'C-Fibre
				// Discount')]")).click();

				driver.findElement(By
						.xpath("//table/tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'C-Fibre Discount')]"))
						.click();
				Thread.sleep(300);
				EleExist = ElementExist(driver, "//*[@value='PPFTTHMPDS" + Discount + "']", DataPath, iCount, sheetnum,
						"Discount metioned in Excel");
				if (EleExist.equals("false")) {
					Thread.sleep(800);
					driver.findElement(By.xpath("//input[@value='Cancel']")).click();
					Norecord = "false";
					return Norecord;
				}
				driver.findElement(By.xpath("//*[@value='PPFTTHMPDS" + Discount + "']")).click();
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("false")) {
					driver.switchTo().parentFrame();
					driver.switchTo().parentFrame();
					driver.findElement(By.xpath("//input[@value='Cancel']")).click();
					Thread.sleep(800);
					PageFocus(driver);
					driver.switchTo().parentFrame();
					driver.findElement(By.xpath("//input[@value='Cancel']")).click();
					Norecord = "false";
					return Norecord;
				}
			}

			if (!Addvas.equals("") && null != Addvas) {
				// driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(
						By.xpath("//tbody/tr/td[@class='border_rightbottom2']/span[contains(text(),'Others')]"))
						.click();
				Thread.sleep(500);
				String[] strvas = Addvas.split(";");
				int vassize = strvas.length;

				for (int f = 0; f < vassize; f++) {
					EleExist = ElementExist(driver, "//span[contains(text(),'" + strvas[f] + "')]", DataPath, iCount,
							sheetnum, strvas[f]);
					if (EleExist.equals("false")) {
						Thread.sleep(800);
						driver.switchTo().parentFrame();
						// PageFocus(driver);
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}
					String strtitle = driver.findElement(By.xpath("//span[contains(text(),'" + strvas[f] + "')]"))
							.getAttribute("title");
					driver.findElement(By.xpath("//*[@value='" + strtitle + "']")).click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {

						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Thread.sleep(800);
						PageFocus(driver);
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value='Cancel']")).click();
						Norecord = "false";
						return Norecord;
					}

				}
			}

			// else{
			// WriteData(Results, iCount,sheetnum ,Poduc, DataPath);
			// WriteData(Usedby, iCount,sheetnum , Completed, DataPath);
			// Norecord="false";
			// driver.findElement(By.xpath("//*[@value='Save']")).click();
			// return Norecord;

			// }

			driver.switchTo().parentFrame();
			PageFocus(driver);
			driver.findElement(By.xpath("//*[@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@value='Cancel']")).click();
				Thread.sleep(800);
				PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.findElement(By.xpath("//input[@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}

		} else {
			WriteData(Results, iCount, sheetnum, Poduc, DataPath);
			WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
			Norecord = "false";

		}

		return Norecord;
	}

	/*
	 * public String BCDeal(WebDriver driver, String Dealtype, String
	 * productitem, String PricePlanId, int months, String Dealcode, String
	 * Imei, String sDefaultPath, String PPD, int iCount, int sheetnum, String
	 * DataPath) throws InterruptedException, InvalidFormatException,
	 * IOException, SAXException, ParserConfigurationException { String
	 * Dealtype1 = Dealtype.toLowerCase(); String[] sColumn = { "Results" };
	 * String[] sColumn1 = { "Used_By" }; String[] sData1 = { "Completed" };
	 * String[] Proderr = { "Prodcut Mentioned Excel does not Exists" };
	 * String[] Priceplanerr = { "Priceplan Mentioned Excel does not Exists" };
	 * String[] Monthsd = { "Months Mentioned Excel does not Exists" }; String[]
	 * Dealerr = { "Deal Code Metioned Does Not Exists" }; String AlertText,
	 * alertmessage = null; String Norecord = "true"; switch (Dealtype1) { case
	 * "sim only": driver.switchTo().parentFrame();
	 * driver.switchTo().frame("mainFrame"); // Actions action = new
	 * Actions(driver); // action.moveToElement(driver.findElement(By.xpath(
	 * "//input[@id='RADIO_controlProductDiv5']"))).doubleClick().build().
	 * perform(); // driver.findElement(By.xpath(
	 * "//span[@id='span_simonly']/input[@id='RADIO_controlProductDiv5'][@value='5']"
	 * )).click();
	 * driver.findElement(By.xpath("//input[@id='RADIO_controlProductDiv5']")).
	 * click(); Thread.sleep(2000); // String productitem ="MPWHLSLPP0A";
	 * driver.findElement( By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']"
	 * )) .click(); // String // sfgd=driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"
	 * +productitem+"']")).getText(); List<WebElement> productlist = driver
	 * .findElements(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option"));
	 * for (int z = 1; z <= productlist.size(); z++) { int prdtsize =
	 * productlist.size();
	 * 
	 * String productid = driver .findElement(By .xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[" + z
	 * + "]")) .getAttribute("value"); if
	 * (productitem.equalsIgnoreCase(productid)) { driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[" + z
	 * + "][@value='" + productid + "']")).click(); break; } if (z == prdtsize)
	 * {
	 * 
	 * WriteData(sColumn, iCount, sheetnum, Proderr, DataPath);
	 * WriteData(sColumn1, iCount, sheetnum, sData1, DataPath); Norecord =
	 * "false"; return Norecord; } } // String months ="24"; //
	 * driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']"
	 * )).click(); // String // sfgd=driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"
	 * +productitem+"']")).getText(); List<WebElement> monthssize = driver
	 * .findElements(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[3]/td[3]/select/option"));
	 * int monthssiz = monthssize.size(); for (int z = 1; z <=
	 * monthssize.size(); z++) {
	 * 
	 * String monthsname = driver .findElement(By .xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[3]/td[3]/select/option[" + z
	 * + "]")) .getAttribute("value"); int monthsnamee =
	 * Integer.parseInt(monthsname); if (months == monthsnamee) {
	 * driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[3]/td[3]/select/option[" + z
	 * + "][@value='" + months + "']")).click(); break; } if (z == monthssiz) {
	 * System.out.println("Productid not Found"); WriteData(sColumn, iCount,
	 * sheetnum, Monthsd, DataPath); WriteData(sColumn1, iCount, sheetnum,
	 * sData1, DataPath); Norecord = "false"; return Norecord; } } // String
	 * PricePlanId ="PPWHLSLTPA0A"; // driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr/td[3]/span/input[@value='postpaid']"
	 * )).click(); // String // sfgd=driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[2]/td[3]/select/option[@value='"
	 * +productitem+"']")).getText(); List<WebElement> PricePlanlist = driver
	 * .findElements(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[4]/td[3]/select/option"));
	 * int pricesize = PricePlanlist.size(); for (int z = 1; z <=
	 * PricePlanlist.size(); z++) {
	 * 
	 * String Prceplanid = driver .findElement(By .xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[4]/td[3]/select/option[" + z
	 * + "]")) .getAttribute("value"); if
	 * (PricePlanId.equalsIgnoreCase(Prceplanid)) { driver.findElement(By.xpath(
	 * "//div[@id='DIV_simonly']/div/table/tbody/tr[4]/td[3]/select/option[" + z
	 * + "][@value='" + PricePlanId + "']")).click(); break; } if (z ==
	 * pricesize) { System.out.println("Productid not Found");
	 * WriteData(sColumn, iCount, sheetnum, Priceplanerr, DataPath);
	 * WriteData(sColumn1, iCount, sheetnum, sData1, DataPath); Norecord =
	 * "false"; return Norecord; } } break;
	 * 
	 * } return Norecord; }
	 */
	public void monthlylimit(WebDriver driver) throws Exception {
		driver.switchTo().frame("mainFrame");
		driver.findElement(By.xpath("//input[@id='selectVML']")).click();
		driver.switchTo().parentFrame();
		// Thread.sleep(1200);
		PageFocus(driver);
		driver.findElement(By.xpath("//input[@value='Save']")).click();
	}

	public void Bundlelimit(WebDriver driver) throws Exception {
		driver.switchTo().frame("mainFrame");
		driver.findElement(By.xpath("//*[@id='setUsageLimit']")).click();
		driver.switchTo().parentFrame();
		PageFocus(driver);

		driver.findElement(By.xpath("//input[@value='Save']")).click();
	}

	public int rowCount(Sheet sheet, Recordset record, ResultSet resultset, String Type) throws Exception {
		int count = 0;
		switch (Type.toUpperCase()) {
		case "EXCEL":
			count = sheet.getPhysicalNumberOfRows();
			break;
		case "FILLO":

			count = record.getCount();
			break;
		case "SQLSERVER":
			int i = 0;
			while (resultset.next()) {
				i++;
			}
			count = i;
		}
		return count;
	}

	public int columnCount(Sheet sheet) throws Exception {
		return sheet.getRow(0).getLastCellNum();
	}

	public void writeData(String[] sColumn, int Row, int shet, String[] sData, String filepath, String Type,
			String sQuery) throws IOException, InvalidFormatException, FilloException, SQLException {
		switch (Type.toUpperCase()) {
		case "EXCEL":
			int CoulmnNo = 0;
			FileInputStream file = new FileInputStream(filepath);
			Workbook wb = WorkbookFactory.create(file);
			sheet = wb.getSheetAt(shet);

			org.apache.poi.ss.usermodel.Cell cell = null;
			Row row = sheet.getRow(shet);

			for (int c = 0; c < sColumn.length; c++) {
				for (int i = 0; i < row.getLastCellNum(); i++) {
					if (row.getCell(i).getStringCellValue().trim().equals(sColumn[c])) {
						CoulmnNo = i;
						Row raw = sheet.getRow(Row);
						cell = raw.createCell(CoulmnNo);
						cell.setCellValue((String) sData[c]);
						break;
					}
				}
			}

			FileOutputStream fileOut = new FileOutputStream(filepath);
			wb.write(fileOut);
			fileOut.close();

			break;
		case "FILLO":
			System.setProperty("ROW", "1");// Table start row
			connect.executeUpdate(sQuery);
			// connect.close();
			break;
		case "SQLSERVER":
			Statement st = conn.createStatement();
			st.execute(sQuery);
		}

	}

	public ResultSet ConnectAndQuerySQLServer(String sDBURL, String sUserName, String sPassword, String sQuery) {

		ResultSet rs = null;

		try {

			String dbURL = sDBURL;
			String user = sUserName;
			String pass = sPassword;
			conn = DriverManager.getConnection(sDBURL, user, pass);
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(sQuery);

		} catch (SQLException ex) {
			ex.printStackTrace();

		}
		return rs;
	}

	public String BCDeal(WebDriver driver, String Dealtype, String productitem, String PricePlanId, String Dealcode,
			String Imei, String sDefaultPath, String PPD, int iCount, int sheetnum, String DataPath) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] NOCustomer = { "NoCustomer" };
		String[] Usedby = { "Used_By" };
		String alrtiter = "true";
		String Norecord = "true";
		String EleExist = "true";
		String EleExists1 = "true";
		String EleExists2 = "true";
		String connet[] = { "Please Enter Connection or Installation Fee for Connect Product In Excel" };
		String Poduc[] = { "Product Information is Null In Excel" };

		driver.findElement(By.xpath("//*[@id='SelectDealButton']")).click();
		driver.switchTo().parentFrame();
		switch (Dealtype) {

		case "Hero":
			Thread.sleep(800);
			PageFocus(driver);
			driver.findElement(By.xpath("//*[@value='HeroDeal']")).click();
			Thread.sleep(600);
			EleExists1 = ElementExist(driver,
					"//*[@id='HeroDealDiv']/div/table/tbody/tr/td[6]/select/option[@value='" + PricePlanId + "']",
					DataPath, iCount, sheetnum, "Priceplan does not exixts at deal level");
			if (EleExists1.equals("false")) {
				Thread.sleep(1000);
				driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				// Norecord="false";
				// return Norecord;
			} else {
				driver.findElement(By.xpath(
						"//*[@id='HeroDealDiv']/div/table/tbody/tr/td[6]/select/option[@value='" + PricePlanId + "']"))
						.click();

			}
			Thread.sleep(1800);
			driver.switchTo().frame("dealListFrame");

			EleExists2 = ElementExist(driver, "//tbody[@class='webfx-columnlist-body']/tr/td", DataPath, iCount,
					sheetnum, "Deal code is not Created for the Priceplan Mention at Select deal");
			if (EleExists2.equals("false")) {
				Thread.sleep(1000);
				driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				// Norecord="false";
				// return Norecord;
			} else {
				// String
				// dealid=driver.findElement(By.xpath("//tbody[@class='webfx-columnlist-body']/tr/td[2]")).getAttribute("title");
				List<WebElement> dealtab = driver.findElements(By.xpath("//tbody[@class='webfx-columnlist-body']/tr"));
				for (int w = 1; w <= dealtab.size(); w++) {
					String dealdes = driver
							.findElement(By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + w + "]/td/input"))
							.getAttribute("dealCode");
					if (dealdes.equals(Dealcode)) {
						String stidno = driver
								.findElement(By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + w + "]/td/input"))
								.getAttribute("id");
						driver.findElement(By.xpath(
								"//tbody[@class='webfx-columnlist-body']/tr[" + w + "]/td/input[@id='" + stidno + "']"))
								.click();
					} else {

						if (w == dealtab.size() && !dealdes.equals(Dealcode)) {
							driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
							String[] strdealcodeerr = { "Deal code Metioned does not Exists at select deal Windowl" };
							WriteData(Results, iCount, sheetnum, strdealcodeerr, DataPath);
							WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
							Norecord = "false";
							return Norecord;
						}
					}
				}
			}
			driver.switchTo().parentFrame();
			driver.findElement(By
					.xpath("//div[@id='HeroDealDiv']/form[@id='HeroDealInfoForm']/div/table/tbody/tr[3]/td[3]/input[@id='IMEI-2']"))
					.sendKeys(Imei);
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}

			driver.findElement(By.xpath("//input[@id='selectDealSave'] [@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}
			Thread.sleep(4000);

			PageFocus(driver);
			PageFocus(driver);
			PageFocus(driver);
			/*
			 * driver.switchTo().parentFrame(); driver.switchTo().parentFrame();
			 */
			driver.findElement(By.xpath("//input[@id='saveBt'][@value='Save']")).click();
			// driver.findElement(By.xpath("//input[@value='Save']")).click();
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}
			break;
		case "Custom":
			PageFocus(driver);
			driver.findElement(By.xpath("//*[@value='CustomDeal']")).click();
			Thread.sleep(1500);
			// driver.switchTo().frame("dealListFrame");
			EleExists1 = ElementExist(driver,
					"//*[@id='customeDealDiv']/div/table/tbody/tr/td[6]/select/option[@value='" + PricePlanId + "']",
					DataPath, iCount, sheetnum, "Priceplan does not exixts at deal level");
			if (EleExists1.equals("false")) {
				Thread.sleep(1000);
				driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			} else {
				driver.findElement(By.xpath("//*[@id='customeDealDiv']/div/table/tbody/tr/td[6]/select/option[@value='"
						+ PricePlanId + "']")).click();

			}
			driver.findElement(
					By.xpath("//div[@id='customeDealDiv']/table[2]/tbody/tr/td[3]/input[@id='customeDealDiv-IMEI2']"))
					.sendKeys(Imei);
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}

			driver.findElement(By.xpath("//input[@id='selectDealSave'] [@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}
			Thread.sleep(1800);
			PageFocus(driver);
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}

			break;
		case "Fixed":
			PageFocus(driver);
			driver.findElement(By.xpath("//*[@value='FixedDeal']")).click();
			Thread.sleep(1000);
			Thread.sleep(400);
			EleExists1 = ElementExist(driver,
					"//*[@id='FixedDealDiv']/div/table/tbody/tr/td[6]/select/option[@value='" + PricePlanId + "']",
					DataPath, iCount, sheetnum, "Priceplan does not exixts at deal level");
			if (EleExists1.equals("false")) {
				Thread.sleep(1000);
				driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			} else {
				driver.findElement(By.xpath(
						"//*[@id='FixedDealDiv']/div/table/tbody/tr/td[6]/select/option[@value='" + PricePlanId + "']"))
						.click();

			}
			Thread.sleep(1500);
			driver.switchTo().frame("fixedDealListFrame");

			EleExists2 = ElementExist(driver, "//tbody[@class='webfx-columnlist-body']/tr/td", DataPath, iCount,
					sheetnum, "Deal code is not Created for the Priceplan Mention at Select deal");
			if (EleExists2.equals("false")) {
				Thread.sleep(1000);
				driver.switchTo().parentFrame();
				// PageFocus(driver);
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			} else {
				// String
				// dealid=driver.findElement(By.xpath("//tbody[@class='webfx-columnlist-body']/tr/td[2]")).getAttribute("title");
				List<WebElement> dealtab = driver.findElements(By.xpath("//tbody[@class='webfx-columnlist-body']/tr"));
				for (int w = 1; w <= dealtab.size(); w++) {
					String dealdes = driver
							.findElement(By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + w + "]/td/input"))
							.getAttribute("dealCode");
					if (dealdes.equals(Dealcode)) {
						String stidno = driver
								.findElement(By.xpath("//tbody[@class='webfx-columnlist-body']/tr[" + w + "]/td/input"))
								.getAttribute("id");
						driver.findElement(By.xpath(
								"//tbody[@class='webfx-columnlist-body']/tr[" + w + "]/td/input[@id='" + stidno + "']"))
								.click();
					} else {

						if (w == dealtab.size() && !dealdes.equals(Dealcode)) {
							driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
							String[] strdealcodeerr = { "Deal code Metioned does not Exists at select deal Windowl" };
							WriteData(Results, iCount, sheetnum, strdealcodeerr, DataPath);
							WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
							Norecord = "false";
							return Norecord;
						}
					}
				}
			}
			driver.switchTo().parentFrame();
			driver.findElement(By
					.xpath("//div[@id='FixedDealDiv']/form[@id='FixedDealInfoForm']/div/table/tbody/tr[3]/td[3]/input[@id='IMEI-2']"))
					.sendKeys(Imei);
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				Norecord = "false";
				return Norecord;
			}

			driver.findElement(By.xpath("//input[@id='selectDealSave'] [@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@id='selectDealCancel'][@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}
			Thread.sleep(1800);
			PageFocus(driver);
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			if (alrtiter.equals("false")) {
				driver.findElement(By.xpath("//input[@value='Cancel']")).click();
				Norecord = "false";
				return Norecord;
			}
			break;
		}
		return Norecord;
	}

}