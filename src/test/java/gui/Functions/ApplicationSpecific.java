package gui.Functions;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.net.InetAddress;
import java.sql.ResultSet;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import javax.xml.parsers.ParserConfigurationException;
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.xml.sax.SAXException;

import com.aventstack.extentreports.ExtentTest;
import com.codoid.products.fillo.Recordset;
import com.gargoylesoftware.htmlunit.javascript.background.JavaScriptExecutor;

public class ApplicationSpecific {
	protected WebElement form;
	private static final String Webdriver = null;
	UtilityFunctions utils = new UtilityFunctions();
	static DataFunctions data = new DataFunctions();
	// public string alrtiter;
	String siderrtext[];
	public String strmsisdnsts;
	public String strprodcttyp;
	public String strproducttype;
	public String productName;
	public String strproid;
	public String strprcplnid;
	public String outputValue;
	public String PriceplanidID;
	public String ListPricepalid1;
	public String strradioid;
	public String strradioidid;
	public String strlogdetails;
	public String dealdescriptin;
	public String strradioidname;
	public String productID;
	public String strradioidvalue;
	public String migrationtyp;
	public String strNorecoed;

	// public WebDriver driver;
	/*****************************************************************************
	 * Function Name: Login adactin Description: Login to the adactin website to
	 * book flights Date Created: 13/09/2017
	 * 
	 * @param sDefaultPath
	 ******************************************************************************/
	// rghfgh sddsdsdsdegerger

	public String appLoginAdactin(WebDriver driver, String sUserName, String sPass, ExtentTest logger,
			String sDefaultPath, String BookHotel) {
		String[] Results = { "Results" };
		String[] Useridnot = { "Norecord Found with the Msisdn You Have Searched" };
		String[] Completed = { "Completed" };
		// String [] NOCustomer={"NoCustomer Found with the Msisdn you have
		// Searched"};
		try {
			if (utils.checkIfObjectIsDisplayed(driver, "username",
					sDefaultPath + "\\Repository\\" + BookHotel + ".xml")) {
				utils.EnterText(driver, "username", sUserName, sDefaultPath + "\\Repository\\" + BookHotel + ".xml");
				utils.EnterText(driver, "password", sPass, sDefaultPath + "\\Repository\\" + BookHotel + ".xml");

				utils.ClickObject(driver, "buttonlogin", sDefaultPath + "\\Repository\\" + BookHotel + ".xml");
				System.out.print("Login Successful");
			} else {
				System.out.print("Login was UnSuccessful");
			}
			if (utils.checkIfObjectIsDisplayed(driver, "InavlidLogin",
					sDefaultPath + "\\Repository\\" + BookHotel + ".xml")) {
				strlogdetails = "false";
				if (strlogdetails.equals("fasle")) {
					return strlogdetails;
				}

				driver.quit();
			}
			strlogdetails = "true";

			Thread.sleep(4000);
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			utils.ExtentLogPassFail(driver, "search", "Login Successful", "Login unSuccessful", logger, true,
					sDefaultPath, "\\Repository\\" + BookHotel + ".xml");

		} catch (Exception e) {
			System.out.println(e.getMessage());
			driver.quit();
		}
		return strlogdetails;
	}

	/*****************************************************************************
	 * Function Name: appSearchHotel Description: Search Available Hotels Date
	 * Created: 13/09/2017
	 * 
	 * @param sheet
	 * @param sheet
	 * @param record
	 * @param Type
	 * @param sDefaultPath
	 * @throws Exception
	 ******************************************************************************/
	public void appSearchHotel(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet, Recordset record,
			ResultSet resultset, String Type, String sDefaultPath, String PPD, String location, int sheetnum)
			throws Exception {
		// String DataPath = ".//data/PPDExcel.xls";
		driver.switchTo().frame("topFrame").switchTo().frame("ifraTopMenu");
		// utils.ClickObject(driver, "clickcustomercare",
		// sDefaultPath+"\\Repository\\"+PPD+".xml");
		if (utils.checkIfObjectIsDisplayed(driver, "customercareExists",
				sDefaultPath + "\\Repository\\" + PPD + ".xml")) {

			Thread.sleep(500);
			utils.ClickObject(driver, "clickcustomercare", sDefaultPath + "\\Repository\\" + PPD + ".xml");

			// Thread.sleep(500);
			utils.ClickObject(driver, "clickcustomercare", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			Thread.sleep(500);
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().frame("leftFrame");
			utils.ClickObject(driver, "clkActivation", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			// Thread.sleep(1000);
			utils.ClickObject(driver, "clkActivationtyp", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			utils.ClickObject(driver, "creditoverride", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			Thread.sleep(2000);
			utils.ClickObject(driver, "searchexistingcustomer", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			Thread.sleep(3000);
			// public String specilLoginNumber;
			// String specilLoginNumber;
			// driver.switchTo().parentFrame();
			// String st =specilLoginNumber;
			// driver.switchTo().parentFrame();
			// driver.switchTo().frame("tar");
			// List<WebElement> Elements =
			// driver.findElements(By.xpath("//frame"));
			// System.out.println("No of Iframes are :"+ Elements.size());
			driver.switchTo().frame(driver.findElement(By.xpath("//iframe[@name='tar']")));
			// System.out.println("No of Iframes are :"+ Element.size());

			// driver.switchTo().frame(Element.get(0));
			System.out.println(driver.getTitle());
			driver.findElement(By.xpath("//*[@id='specilLoginNumber']")).sendKeys("184214");
			// driver.switchTo().frame(driver.findElement(By.tagName("iframe")));
			// driver.switchTo().frame("mainFrame").switchTo().frame(index);
			// driver.switchTo().frame("tar");

			// driver.switchTo().frame(driver.findElement(By.xpath("//*[@id='form1']")));
			driver.findElement(By.xpath("//*[@id='specilLoginNumber']")).sendKeys("184214");
			form.findElement(By.xpath("./descendant::input[@id=//*[@id=\"" + "specilLoginNumber" + "\"]]"))
					.sendKeys("3454");
			String Parentwindow = driver.getWindowHandle();
			Set<String> handler = driver.getWindowHandles();
			int windowsize = handler.size();
			Iterator<String> it = handler.iterator();
			String Childwindow = it.next();

			if (Parentwindow.equalsIgnoreCase(Parentwindow)) {
				driver.switchTo().window(Parentwindow);
				Thread.sleep(1000);
				System.out.println(driver.getTitle());
				utils.EnterText(driver, "ExistingCustomer",
						data.getCellData("IDNUMBER", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
			}
			String typeofact = data.getCellData("Type of Activation", iCount, sheet, record, resultset, Type);
			if (typeofact.equals("Upfront")) {
				utils.ClickObject(driver, "clkUpfrontCheckbox", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.ClickObject(driver, "clkteleCheckbox", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			} else if (typeofact.equals("Normal")) {
				utils.ClickObject(driver, "clkteleCheckbox", sDefaultPath + "\\Repository\\" + PPD + ".xml");
			} else {
				driver.close();
			}
			// Thread.sleep(2000);
			// utils.ClickObject(driver, "IDTYPE",
			// sDefaultPath+"\\Repository\\"+PPD+".xml");
			utils.SelectTextUsingValue(driver, "IDTYPESELECTION",
					data.getCellData("IDTYPE", iCount, sheet, record, resultset, Type),
					sDefaultPath + "\\Repository\\" + PPD + ".xml");
			// Thread.sleep(1000);
			utils.EnterText(driver, "IDNUMBER", data.getCellData("IDNUMBER", iCount, sheet, record, resultset, Type),
					sDefaultPath + "\\Repository\\" + PPD + ".xml");
			utils.EnterText(driver, "FIRSTNAME", data.getCellData("FIRSTNAME", iCount, sheet, record, resultset, Type),
					sDefaultPath + "\\Repository\\" + PPD + ".xml");
			try {
				data.alert(driver, location, iCount, sheetnum);

			} catch (Exception e) {
				utils.EnterText(driver, "LASTNAME",
						data.getCellData("LASTNAME", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				// utils.ClickObject(driver, "GENDERTYPE",
				// sDefaultPath+"\\Repository\\"+PPD+".xml");
				utils.SelectTextUsingValue(driver, "GENDERTYPE",
						data.getCellData("GENDER", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.ClickObject(driver, "DATEOFBIRTH", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				// driver.manage().timeouts().implicitlyWait(100,
				// TimeUnit.SECONDS);
				Thread.sleep(4000);
				// utils.ClickObject(driver, "DOBOK",
				// sDefaultPath+"\\Repository\\"+PPD+".xml");
				long wait = 1000;
				// String date="1-6-1983";
				String date = data.getCellData("DOB", iCount, sheet, record, resultset, Type);
				utils.BirthDateFunctions(driver, date, sDefaultPath, "PPD");
				// driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				utils.EnterText(driver, "TELEPHOENWORK",
						data.getCellData("TELEPHOENWORK", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.EnterText(driver, "TELEPHONEHOME",
						data.getCellData("TELEPHONEHOME", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.EnterText(driver, "CELLNUMBER",
						data.getCellData("CELLNUMBER", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.SelectTextUsingValue(driver, "RESIDENTTYPE",
						data.getCellData("RESIDENTTYPE", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				// driver.findElement(By.xpath("//input[@class='yminputfocus'][@tabindex='1']")).click();
				utils.ClickObject(driver, "EMPDURATION", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				String emdate = data.getCellData("EMPLOYDURATION", iCount, sheet, record, resultset, Type);

				// driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'/custcare/js/newcalendar/DatePicker/My97DatePicker.htm')]")));
				utils.EMPDURDATE(driver, emdate, sDefaultPath, "PPD");
				Thread.sleep(1000);
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				utils.EnterText(driver, "MONTHLYSALARY",
						data.getCellData("MONTHLYSALARY", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.ClickObject(driver, "LINKTOADDRESS", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				Thread.sleep(1000);
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_ALT);
				robot.keyPress(KeyEvent.VK_F4);
				robot.keyRelease(KeyEvent.VK_ALT);
				robot.keyRelease(KeyEvent.VK_F4);
				utils.ClickObject(driver, "RETRIVEADDRESS", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				Thread.sleep(3000);
				String dealtyperadio = data.getCellData("DEALTYPE", iCount, sheet, record, resultset, Type);
				String prodtyperadio = data.getCellData("Producttype", iCount, sheet, record, resultset, Type);
				utils.DealtypeRadio(driver, dealtyperadio, sDefaultPath, "PPD");
				utils.ProducttypeRadio(driver, prodtyperadio, sDefaultPath, "PPD");
				String Product = data.getCellData("PRODUCT", iCount, sheet, record, resultset, Type);
				String Contractterm = data.getCellData("Contractterm", iCount, sheet, record, resultset, Type);
				String Priceplan = data.getCellData("Priceplan", iCount, sheet, record, resultset, Type);

				// utils.SelectTextUsingValue(driver, "GENDERTYPE",
				// data.getCellData("PRODUCT",iCount,sheet,
				// record,resultset,Type),
				// sDefaultPath+"\\Repository\\"+PPD+".xml");
				utils.dropdown(driver, "//*[@id='select_main_product5']/option", Product);
				utils.dropdown(driver, "//*[@id='select_contractPeriod5']/option", Contractterm);
				utils.dropdown(driver, "//*[@id='select_mainPrivilege5']/option", Priceplan);
				utils.EnterText(driver, "Msisdn", data.getCellData("Msisdn", iCount, sheet, record, resultset, Type),
						sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.ClickObject(driver, "Sim", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				utils.ClickObject(driver, "SELECTVAS", sDefaultPath + "\\Repository\\" + PPD + ".xml");
				Thread.sleep(2000);
				data.Popup(driver);
				driver.findElement(
						By.xpath("//td[@class='border_rightbottom2']/span[contains(text(), 'C-Fibre Discount')]"))
						.click();
				Thread.sleep(2000);

				// empdate
				// String empdate=data.getCellData("EXPIRYDATE",iCount,sheet,
				// record,resultset,Type);
				// EMPDURATIONFunction(driver,date,sDefaultPath,"PPD");
			}
			//// driver.findElement(By.xpath("//*[@id='idType']/option[i]"));
			//// utils.SelectTextUsingValue(driver, "location",
			//// data.getCellData("Location",iCount,sheet, record, resultset,
			//// Type), sDefaultPath+"\\Repository\\"+PPD+".xml");
			//// utils.SelectTextUsingValue(driver, "hotels",
			//// data.getCellData("Hotels",iCount,sheet, record,resultset,
			//// Type), sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.SelectTextUsingValue(driver, "roomtype",
			//// data.getCellData("RoomType",iCount,sheet, record,resultset,
			//// Type), sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.SelectTextUsingVisibeText(driver, "roomnumbers",
			//// data.getCellData("NumberOfRooms",iCount,sheet,
			//// record,resultset, Type),
			//// sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.ClearObject(driver, "checkindate",
			//// sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.EnterText(driver, "checkindate",
			//// data.getCellData("CheckInDate",iCount,sheet, record,resultset,
			//// Type), sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.ClearObject(driver, "checkoutdate",
			//// sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.EnterText(driver, "checkoutdate",
			//// data.getCellData("CheckOutDate",iCount,sheet, record,resultset,
			//// Type), sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.SelectTextUsingVisibeText(driver, "adlutsperroom",
			//// data.getCellData("AdultsPerRoom",iCount,sheet,
			//// record,resultset, Type),
			//// sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.SelectTextUsingVisibeText(driver, "childperoom",
			//// data.getCellData("ChildrenPerRoom",iCount,sheet,
			//// record,resultset, Type),
			//// sDefaultPath+"\\Repository\\BookHotel.xml");
			//// utils.ClickObject(driver, "submit",
			//// sDefaultPath+"\\Repository\\BookHotel.xml");
			//// //utils.ExtentLogPassFail(driver, "continue", "Hotel Search
			//// Successful", "Hotel Search unSuccessful", logger, true,
			//// sDefaultPath,"\\Repository\\BookHotel.xml");
		}

	}

	public String Migration_PostpaidtoPostpaid(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet,
			Recordset record, ResultSet resultset, String Type, String sDefaultPath, String PPD, String location,
			String DataPath, int sheetnum) throws Exception {
		data.windowhandler(driver);
		driver.switchTo().frame("topFrame");
		utils.ClickObject(driver, "NextCustomer", sDefaultPath + "\\Repository\\" + PPD + ".xml");
		driver.switchTo().parentFrame();
		// data.Existingcustomer(driver,iCount, logger,
		// sheet,record,resultset,Type,sDefaultPath,"PPD",location,DataPath);
		String[] existingvalues = data.Existingcustomer(driver, iCount, logger, sheet, record, resultset, Type,
				sDefaultPath, "PPD", location, DataPath, sheetnum);
		;
		strmsisdnsts = existingvalues[0];
		strprodcttyp = existingvalues[1];
		strNorecoed = existingvalues[2];
		migrationtyp = "MigrPosttoPost";
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] NOCustomer = { "NoCustomer" };
		String[] Usedby = { "Used_By" };
		if (strNorecoed == "false") {
			return strNorecoed;
		}
		if (!strmsisdnsts.equals("Active")) {
			String[] strmsisdnst = { "strmsisdnsts" };
			data.WriteData(Results, iCount, sheetnum, strmsisdnst, DataPath);
			data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
		} else if (!strprodcttyp.equals("Postpaid")) {
			String[] strproducttype = { "strprodcttyp" };
			data.WriteData(Results, iCount, sheetnum, strproducttype, DataPath);
			data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
		} else {
			data.migrationtype(driver, sDefaultPath, PPD, migrationtyp, sheetnum);
			data.creditoverpageMig(driver);
			String productID = data.getCellData("Main_Product_ID", iCount, sheet, record, resultset, Type);
			String ProductName = data.getCellData("Product_Name", iCount, sheet, record, resultset, Type);
			if (null != productID && !(productID.equals("")) && null != ProductName && !(ProductName.equals(""))) {
				outputValue = data.prodseletion(driver, iCount, logger, sheet, record, resultset, Type, sDefaultPath,
						"PPD", location, DataPath, sheetnum);

				if (!outputValue.equals(productID)) {
					String[] strProdidnot = { "Product does not Exist" };
					data.WriteData(Results, iCount, sheetnum, strProdidnot, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					driver.findElement(By
							.xpath("//form[@name='selectProductForm']/table[2]/tbody/tr/td/input[@type='button'][@value='Cancel']"))
							.click();
				} else {
					String PriceplanidID = data.getCellData("PricePlanID", iCount, sheet, record, resultset, Type);
					if (null != PriceplanidID && !(PriceplanidID.equals(""))) {
						ListPricepalid1 = data.VasandPricepalnsel(driver, iCount, logger, sheet, record, resultset,
								Type, sDefaultPath, "PPD", location, DataPath, sheetnum);

						if (!ListPricepalid1.matches(PriceplanidID)) {
							String[] strPricepalidnot = { "Priceplan does not Exist" };
							data.WriteData(Results, iCount, sheetnum, strPricepalidnot, DataPath);
							data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
							driver.switchTo().parentFrame();

							driver.findElement(By.xpath("//input[@id='Cancel']")).click();
						} else {

							Thread.sleep(2000);
							// driver.findElement(By.xpath("//input[@id='saveBt']")).click();
							// driver.switchTo().defaultContent();
							// driver.switchTo().parentFrame();
							data.windowhandler(driver);

							driver.switchTo().frame("mainFrame");
							driver.switchTo().frame("rightCFrame");
							driver.switchTo().frame("tabContenIfra");
							driver.switchTo().frame("tabIframe0");
							// driver.switchTo().frame(driver.findElement(By.xpath("//iframe[contains(@src,'/custcare/custsvc/basebusiness.*)]")));
							driver.findElement(By.xpath("//input[@name='nextBt'][@id='btnNext']")).click();
							Thread.sleep(1000);
							data.windowhandler(driver);

							Thread.sleep(1000);
							driver.findElement(By.xpath("//input[@id='btnCommit']")).click();
							Thread.sleep(2000);

							data.windowhandler(driver);
							driver.switchTo().frame("mainFrame");
							driver.switchTo().frame("rightCFrame");
							driver.switchTo().frame("tabContenIfra");
							driver.switchTo().frame("tabIframe0");
							driver.findElement(By.xpath("//input[@id='button_print']")).click();

						}
						driver.findElement(By.xpath("//span")).click();

					} else {
						data.windowhandler(driver);
						driver.switchTo().parentFrame();
						String[] strproducttype = { "PriceplanID Can't be null Please Check the Excel" };
						data.WriteData(Results, iCount, sheetnum, strproducttype, DataPath);
						data.WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
					}
				}
			} else {
				driver.switchTo().parentFrame().switchTo().parentFrame().switchTo().parentFrame();

				String[] strproducttype = { "Prodcut Id Can't be null or Product Name Please Check the Excel" };
				data.WriteData(Results, iCount, sheetnum, strproducttype, DataPath);
				data.WriteData(Usedby, iCount, sheetnum, Completed, DataPath);
			}
		}
		return strNorecoed;
	}

	public String QuickActivation(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet, Recordset record,
			ResultSet resultset, String Type, String sDefaultPath, String QuickActivation, String location, String DataPath,
			int sheetnum) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] UsedBy = { "Used_By" };
		int strContractTem = 0;
		// String strIMEIe;
		String[] NOCustomer = { "NoCustomer" };
		String dealserr = "true";
		String vaserr = "true";
		String alrtiter = "true";
		String strDurationOfEmp = data.getCellData("EmpDuration", iCount, sheet, null, null, "Excel");
		String strIDNumber = data.getCellData("ID_Number", iCount, sheet, null, null, "Excel");
		String strCustomer = data.getCellData("Customer", iCount, sheet, null, null, "Excel");
		String strTypeOfActivation = data.getCellData("ActType", iCount, sheet, null, null, "Excel");
		String strIDtype = data.getCellData("ID_Type", iCount, sheet, null, null, "Excel");
		// String strIDNumber=data.getCellData("Customer",1,sheet, null,null,
		// "Excel");
		String strOrgID = data.getCellData("Sales_Force_ID", iCount, sheet, null, null, "Excel");
		String strFirstName = utils.generateRandomString(9);
		String strGender = data.getCellData("Gender", iCount, sheet, null, null, "Excel");
		String strProduct_Type = data.getCellData("Product_Type", iCount, sheet, null, null, "Excel");
		String strPricePlanId = data.getCellData("PricePlanId", iCount, sheet, null, null, "Excel");
		String strPricePlanName = data.getCellData("PricePlanName", iCount, sheet, null, null, "Excel");
		String strProductID = data.getCellData("ProductID", iCount, sheet, null, null, "Excel");
		String strProductName = data.getCellData("ProductName", iCount, sheet, null, null, "Excel");
		String strContractTerm = data.getCellData("Contract_Term", iCount, sheet, null, null, "Excel");
		String strSelectDeal = data.getCellData("Select_Deal", iCount, sheet, null, null, "Excel");
		String strmsisdn = data.getCellData("Msisdn", iCount, sheet, null, null, "Excel");
		String strsim = data.getCellData("Sim", iCount, sheet, null, null, "Excel");
		// String stremail=data.getCellData("Email",iCount,sheet, null,null,
		// "Excel");
		String strConnectVas = data.getCellData("ConnectVas", iCount, sheet, null, null, "Excel");
		String strVas = data.getCellData("Vas", iCount, sheet, null, null, "Excel");
		String Cash = "Cash";
		String Strclienttyp = "Employee";
		String NoDispatch = "No Dispatch";
		if (!strContractTerm.equals("")) {
			strContractTem = Integer.parseInt(strContractTerm);
		}

		String strTelWork = utils.genRandnumber(8);
		strTelWork = "01" + strTelWork;
		String strCell = utils.genRandnumber(9);
		strCell = "01" + strCell;
		String strTelHome = utils.genRandnumber(8);
		strTelHome = "01" + strTelHome;
		String strBankAccType = "Savings";
		String strLastName = utils.generateRandomString(8);
		;
		String strDateOfBirth = data.getCellData("Date_Of_Birth", 1, sheet, null, null, "Excel");
		String strMonthlySalary = utils.genRandnumber(4);
		strMonthlySalary = "1" + strMonthlySalary;
		String strTypeOfResidence = "Rent";
		String strProvince = "Gauteng";
		String strRoadNumber = "1";
		String strRoadName = "Buffalo Road";
		String strSuburb = "Bronberrik";
		String strCity = "Centurion";
		String strPostCode = "0157";
		String strTitle = "Mr.";
		String AlertText;
		String strTypeOfResident = "Tenant";
		String strDeal_Code = data.getCellData("Deal_Code", iCount, sheet, null, null, "Excel");
		String strIMEI = data.getCellData("IMEI", iCount, sheet, null, null, "Excel");
		// if(!strIMEI.equals("")){
		// strIMEIe=Long.parseLong(strIMEI);
		// }
		String strCountry = data.getCellData("Country_of_Issue", iCount, sheet, null, null, "Excel");
		String strExpiryDate = data.getCellData("Expire_date", iCount, sheet, null, null, "Excel");
		String strCompanyName = utils.generateRandomString(13);
		strCompanyName = "iLAB " + strCompanyName;
		// String PaymentAccountList=data.getCellData("Customer",1,sheet,
		// null,null, "Excel");
		String strDebitDate = data.getCellData("Debit_Date", iCount, sheet, null, null, "Excel");
		String strEmail = utils.generateRandomString(6);
		String strEmail1 = utils.genRandnumber(3);
		String strEmail3 = strEmail + strEmail1;
		String strEmail4 = strEmail3 + "@gmail.com";
		strCustomer = data.getCellData("Customer", iCount, sheet, null, null, "Excel");
		String strDiscount = data.getCellData("Discount", iCount, sheet, null, null, "Excel");

		if (strCustomer.matches("New")) {
			try {
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("topFrame");
				driver.switchTo().frame("ifraTopMenu");
				Thread.sleep(1000);
				// data.ElementExist(driver,"//a[text()='Customer
				// Care']",DataPath,iCount,sheetnum);
				/* Customer Care_______________________ */
				// Actions action = new Actions(driver);
				// action.moveToElement(driver.findElement(By.xpath("//*[@id='Customer
				// Care']"))).doubleClick().build().perform();
				utils.ClickObject(driver, "customercare", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				utils.ClickObject(driver, "customercare", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");

				Thread.sleep(500);
				/* Switch to LeftFrame */
				driver.switchTo().parentFrame();/*
												 * Note: we got to switch back
												 * to the page
												 */
				driver.switchTo().parentFrame();
				driver.switchTo().frame("leftFrame");
				Thread.sleep(800);
				// *Activation_______________________*/
				//driver.findElement(By.xpath("//*[@id='div_Aanmelden_WEB']")).click();
				utils.ClickObject(driver, "activation", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				
				//driver.findElement(By.xpath("//*[contains(text(),'Quick Activation')]")).click();
				utils.ClickObject(driver, "quickactivation", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				Thread.sleep(1000);
				if (strTypeOfActivation.matches("Normal")) {
					//driver.findElement(By.xpath("//*[@id='creditVetting']")).click();
					utils.ClickObject(driver, "creditvetting", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.switchTo().parentFrame();
					utils.ClickObject(driver, "telesales", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					
					//driver.findElement(By.xpath("//*[@id='telesales']")).click();
				} else {
					//driver.findElement(By.xpath("//*[@id='creditVetting']")).click();
					utils.ClickObject(driver, "creditvetting", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='upfrontPaymentType']")).click();
					utils.ClickObject(driver, "upfrontPaymentType", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				}
				/* Quick Vetting_____________________________ */

				if (!strIDtype.equals("") | null != strIDtype) {
					
					driver.findElement(
							By.xpath("//div[@id='quickVettingDIV']/div/table/tbody/tr/td[2]/select/option[@value='"
									+ strIDtype + "']"))
							.click();
				} else {
					String[] strIDtypeerr = { "IDType DOes not Exists" };
					data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
			
				if (!strIDNumber.equals("") | null != strIDNumber) {
					// driver.findElement(By.xpath("//*[@id='page-container']")).click();
					utils.EnterText(driver, "idnumber", strIDNumber,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("idNumber")).sendKeys(strIDNumber);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "IDNUMBER DOes not Exists" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				//driver.findElement(By.xpath("//div[@id='quickVettingDIV']/div/table/tbody/tr[2]/td[2]")).click();
				
				utils.ClickObject(driver, "openclick", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				// driver.switchTo().parentFrame();
				Thread.sleep(500);
				/*alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("fasle")) {

					strNorecoed = "false";
					return strNorecoed;
				}*/
				WebDriverWait wait = new WebDriverWait(driver,
						1  );
               try{
				wait.until(ExpectedConditions.alertIsPresent());
				Alert alert = driver.switchTo().alert();
				AlertText = driver.switchTo().alert().getText();
				if (AlertText.equals("There is an existing customer in the system, Are you sure you want to choose?")) {
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					data.PageFocus(driver);
					driver.switchTo().frame("mainFrame");
					utils.ClickObject(driver, "close", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//utils.ClickObject(driver, "close", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("//*[text()='Close']")).click();
					//driver.findElement(By.xpath("//*[text()='Close']")).click();
					strNorecoed = "false";
					return strNorecoed;
				} else {
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}

				}}catch (TimeoutException eTO) {

					strNorecoed = "true";
					// return strNorecoed;
				}
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");
				if (!strIDtype.equals("") | null != strIDtype) {
					driver.findElement(
							By.xpath("//div[@id='quickVettingDIV']/div/table/tbody/tr[2]/td[2]/select/option[text()='"
									+ strCountry + "']"))
							.click();
				} else {
					String[] IDNUMBER = { "Country name mentioned In Excel does not Exists" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				// Thread.sleep(500);
				// driver.findElement(By.id("idNumber")).sendKeys(strIDNumber);
				// driver.findElement(By.xpath("//div[@id='quickVettingDIV']/div[2]/table/tbody/tr/td")).click();
				// alrtiter=utils.GeneralAlert(driver,iCount,DataPath,sheetnum);
				// if(alrtiter.equals("false")){
				// strNorecoed="false";
				// return strNorecoed;
				// }
				/*
				 * Credit Vetting____________________
				 * ___________________________________________
				 */
				// driver.findElement(By.xpath("//*[@id='salesForceID']")).click();
				// String
				// sdgsd=driver.findElement(By.xpath("//*[@id='salesForceID']")).getAttribute("value");
				if ((driver.findElement(By.xpath("//*[@id='salesForceID']")).getAttribute("value"))
						.equals("Dealer Centre")) {
					utils.ClickObject(driver, "salesforceid", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("")).click();

					Thread.sleep(500);
					driver.switchTo().parentFrame();
					Thread.sleep(200);
					data.Organisation(driver, strOrgID);
				}
				data.PageFocus(driver);
				// utils.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strFirstName.equals("") | null != strFirstName) {
					utils.EnterText(driver, "FirstName", strFirstName,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("firstName")).sendKeys(strFirstName);
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strFirstNameerr = { "First name mentioned In Excel does not Exists or Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, strFirstNameerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				// data.PageFocus(driver);
				// driver.switchTo().frame("mainFrame");
				if (!strGender.equals("") | null != strGender) {
					driver.findElement(By.xpath("//tr[3]/td[2]/select/option[text()='" + strGender + "']")).click();
				} else {
					String[] strGendererr = { "Gender mentioned id can't be null" };
					data.WriteData(Results, iCount, sheetnum, strGendererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strTelWork.equals("") | null != strTelWork) {
					utils.EnterText(driver, "telephoneWork", strTelWork,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='telephoneWork']")).sendKeys(strTelWork);
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strGendererr = { "Work Telephone  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strGendererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strTelHome.equals("") | null != strTelHome) {
					utils.EnterText(driver, "TelHome", strTelHome,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='telephoneHome']")).sendKeys(strTelHome);
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strTelHomeerr = { "Home Telephone  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strTelHomeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strCell.equals("") | null != strCell) {
					
					utils.EnterText(driver, "Cell", strCell,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='cellNo']")).sendKeys(strCell);
					Thread.sleep(200);
					
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strCellerr = { "Cellnumber  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCellerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strBankAccType.equals("") | null != strBankAccType) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strBankAccType + "']")).click();
				} else {
					String[] strBankAccTypeerr = { "BankAccType  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strBankAccTypeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strLastName.equals("") | null != strLastName) {
					utils.EnterText(driver, "LastName", strLastName,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("lastName")).sendKeys(strLastName);
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strLastNameerr = { "BankAccType  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strLastNameerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");
				// DateOfBirth
				if (!strDateOfBirth.equals("") | null != strDateOfBirth) {
					utils.ClickObject(driver, "dateOfBirth", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("dateOfBirth")).click();
					Thread.sleep(100);
					data.BirtDayFunctions(driver, strDateOfBirth, "BirthDate");
				} else {
					String[] strDateOfBirtherr = { "DateOfBirth  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strDateOfBirtherr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strTypeOfResident.equals("") | null != strTypeOfResident) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strTypeOfResident + "']")).click();
				} else {
					String[] strTypeOfResidenterr = { "TypeOfResident  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strTypeOfResidenterr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				// DurationOfEmp
				if (!strDurationOfEmp.equals("") | null != strDurationOfEmp) {
					utils.ClickObject(driver, "durationofEmployment", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("durationofEmployment")).click();
					Thread.sleep(200);
					data.BirtDayFunctions(driver, strDurationOfEmp, "MonthYearDate");
				} else {
					String[] strDurationOfEmployment = { "DurationOfEmployment  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strDurationOfEmployment, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strMonthlySalary.equals("") | null != strDurationOfEmp) {
					utils.EnterText(driver, "monthlySalary", strMonthlySalary,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("monthlySalaryR")).sendKeys(strMonthlySalary);
				} else {
					String[] strMonthlySalerr = { "MonthlySalary  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strMonthlySalerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				/*
				 * Residential
				 * Address___________________________________________________________
				 */
				if (!strTypeOfResidence.equals("") | null != strTypeOfResidence) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strTypeOfResidence + "']")).click();
				} else {
					String[] strTypeOfResidenceerr = { "TypeOfResidence  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strTypeOfResidenceerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strProvince.equals("") | null != strProvince) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strProvince + "']")).click();
				} else {
					String[] strProvinceerr = { "Province  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strProvinceerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strRoadNumber.equals("") | null != strRoadNumber) {
					utils.EnterText(driver, "RoadNumber", strRoadNumber,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("streetNo")).sendKeys(strRoadNumber);
				} else {
					String[] strRoadNumbererr = { "RoadNumber  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strRoadNumbererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strRoadName.equals("") | null != strRoadName) {
					utils.EnterText(driver, "AddressLine", strRoadName,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("resAddressLine2Temp")).sendKeys(strRoadName);
				} else {
					String[] strRoadNumbererr = { "RoadName  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strRoadNumbererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strSuburb.equals("") | null != strSuburb) {
					utils.EnterText(driver, "suburb", strSuburb,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("suburb")).sendKeys(strSuburb);
				} else {
					String[] strSuburberr = { "Suburb  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strSuburberr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strCity.equals("") | null != strCity) {
					utils.EnterText(driver, "City", strCity,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("city")).sendKeys(strCity);
				} else {
					String[] strCityerr = { "City  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCityerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strPostCode.equals("") | null != strPostCode) {
					utils.EnterText(driver, "PostCode", strPostCode,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("postalCode")).sendKeys(strPostCode);
				} else {
					String[] strPostCodeerr = { "strPostCode  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strPostCodeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(1000);
				// driver.switchTo().frame("mainFrame");
				if (!(driver.findElement(By.xpath("//*[@id='subsSalesForceID']")).getAttribute("value"))
						.equals("Dealer Centre")) {
					utils.ClickObject(driver, "subsSalesForceID", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='subsSalesForceID']")).click();
					//driver.findElement(By.xpath("//*[@id='subsSalesForceID']")).click();
					Thread.sleep(500);
					driver.switchTo().parentFrame();
					data.Organisation(driver, strOrgID);
				}
				data.PageFocus(driver);
				dealserr = data.QuickDeals(driver,strProduct_Type, strSelectDeal, strProductID, strPricePlanId, strContractTerm,
						strDeal_Code, strIMEI, sDefaultPath, QuickActivation, iCount, sheetnum, DataPath);
				if (dealserr.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}

				// data.PageFocus(driver);
				// driver.switchTo().parentFrame();
				// driver.switchTo().frame("mainFrame");
				if (!strmsisdn.equals("") | null != strmsisdn) {
					utils.EnterText(driver, "msisdn", strmsisdn,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					
					//driver.findElement(By.id("txtMsisdn")).sendKeys(strmsisdn);
					
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strmsisdnerr = { "strmsisdn  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strmsisdnerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				utils.ClickObject(driver,  "Sim", sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				Thread.sleep(500);
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);

				if (alrtiter.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strsim.equals("") | null != strsim) {
					utils.EnterText(driver, "Sim", strsim,  sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("ssnSuffix")).sendKeys(strsim);
					//Thread.sleep(300);
					data.PageFocus(driver);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strsimerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strsimerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				data.PageFocus(driver);
				vaserr = data.Vas(driver, strDiscount, strEmail4, strProductName, strVas, strConnectVas, iCount,
						sheetnum, DataPath);

				if (vaserr.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(1000);
				//data.PageFocus(driver);
				//data.monthlylimit(driver);
				data.PageFocus(driver);
				data.Bundlelimit(driver);
				// driver.switchTo().parentFrame();
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");
				/*
				 * Customer
				 * Details______________________________________________________________
				 */

				utils.ClickObject(driver, "installationAddress", 
			sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='installationAddress']")).click();
				Thread.sleep(300);
				utils.ClickObject(driver, "installationAddressValue", 
						sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='installationAddressValue']")).click();
				Thread.sleep(300);
				driver.findElement(
						By.xpath("//select[@name='consumerCustInfo.title']/option[text()='" + strTitle + "']")).click();
				driver.findElement(
						By.xpath("//select[@name='consumerCustInfo.clientTypeInfo.clientType']/option[text()='"
								+ Strclienttyp + "']"))
						.click();
				// driver.findElement(By.xpath("//td[2]/select/option[text()='"+strTitle+"']")).click();
				// driver.findElement(By.xpath("//td[2]/select/option[text()='"+strCountry+"']")).click();
				// Expiry date
				if (!strExpiryDate.equals("") | null != strExpiryDate) {
					utils.ClickObject(driver, "expiryDate", 
							sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
					//driver.findElement(By.id("expiryDate")).click();
					Thread.sleep(1000);
					data.BirtDayFunctions(driver, strExpiryDate, "ExpiryDate");
				} else {
					String[] strExpiryDateerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strExpiryDateerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strCompanyName.equals("") | null != strCompanyName) {
					utils.EnterText(driver, "companyName", strCompanyName,  
							sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");

					//driver.findElement(By.id("companyName")).sendKeys(strCompanyName);
				} else {
					String[] strCompanyNameeerr = { "Companyname  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCompanyNameeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (strTypeOfActivation.equals("Normal")) {
					if (!Cash.equals("") | null != Cash) {
						driver.findElement(
								By.xpath("//select[@id='account_paymentMethod']/option[text()='" + Cash + "']"))
								.click();
					}
				} else {
					String[] strCompanyNameeerr = { "Companyname  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCompanyNameeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!NoDispatch.equals("") | null != NoDispatch) {
					driver.findElement(By
							.xpath("//select[@id='billFormatInfo_deliveryFormat']/option[text()='" + NoDispatch + "']"))
							.click();
				} else {
					String[] strNoDispatcherr = { "NoDispatch  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strNoDispatcherr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strDebitDate.equals("") | null != strDebitDate) {
					driver.findElement(
							By.xpath("//select[@id='billInfo_fixedDate']/option[@value='" + strDebitDate + "']"))
							.click();
				} else {
					String[] strDebitDateeerr = { "DebitDate  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strDebitDateeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				utils.ClickObject(driver, "Next", 
						sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='next']")).click();
				Thread.sleep(300);
				WebDriverWait wait1 = new WebDriverWait(driver,
						1/* timeout in seconds */);
				try{
				wait1.until(ExpectedConditions.alertIsPresent());
				Alert alert1 = driver.switchTo().alert();
				String AlertText1 = driver.switchTo().alert().getText();
				if (AlertText1.equals(
						"For a contract buyout customer click OK to select and enter the EPIC Gift Card serial number. Click Cancel if not a contract buyout customer.")) {
					alert1.dismiss();
				} else {
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}}catch (TimeoutException eTO1) {

					strNorecoed = "true";
					// return strNorecoed;
				}
				
				Thread.sleep(1200);
				utils.EnterText(driver, "Discounttxtarea", "sgsfgasf",  
						sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				
				//driver.findElement(
						//By.xpath("//div[@id='div_discount_drop_down_panel']/table/tbody/tr[2]/td[2]/textarea"))
						//.sendKeys("sgsfgasf");

				utils.ClickObject(driver, "EmailTemp", 
			sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				
				//driver.findElement(By.xpath("//*[@id='sendEmailTemp']")).click();
				//driver.findElement(By.xpath("//*[@id='button_print']")).click();
				utils.ClickObject(driver, "buttonprint", 
						sDefaultPath + "\\Repository\\" + QuickActivation + ".xml");
				Thread.sleep(2000);
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}
				data.PageFocus(driver);
				data.PageFocus(driver);
				//String strtitl=driver.getTitle();
				//driver.findElement(By.xpath("//*[@title='"+strtitl+"']")).click();
				Actions action = new Actions(driver);
				//Actions action= new Actions(driver);
				action.sendKeys(Keys.TAB).perform();
                //action.sendKeys(keys.TAB);
				action.keyDown(Keys.CONTROL).sendKeys(Keys.chord("A")).keyUp(Keys.CONTROL).perform();
				driver.close();
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}

				/** Navigate_____________________________________________________________________ */
				
			} catch (Exception e) {
				// TODO: handle exception
			}
		} else {
			driver.findElement(By.id("searchOldCust")).click();
			Thread.sleep(1000);
			driver.switchTo().frame(driver.findElement(By.xpath(
					"//iframe[@src='/custcare/custsvc/basebusiness/install/queryOPCAction.do?act=queryOldCusts&queryID=']")));
			Thread.sleep(1000);
			driver.switchTo().frame(driver.findElement(By.xpath(
					"//iframe[@src='/custcare/custsvc/servicequery/customerQueryAction.do?method=initPage&retFlag=retCustomerId']")));
			Thread.sleep(1000);
			driver.findElement(By.name("specilLoginNumber")).sendKeys("0521900286");
			Thread.sleep(1000);
			driver.findElement(By.name("search")).click();
			// Checkpoint needed
			Actions action = new Actions(driver);
			action.moveToElement(driver.findElement(By.xpath("//table[@id='customerList']/tbody/tr/td[5]")))
					.doubleClick().build().perform();
			Thread.sleep(1000);
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
		}

		/*driver.findElement(By.id("next")).click();
		Thread.sleep(1000);

		// utils.PaymentAccountList(driver,"Yes");

		// utils.PaymentAccountList(driver);
		Thread.sleep(2000);
		
		 * Account
		 * Information___________________________________________________________
		 
		/// driver.findElement(By.name("accounts[0].creditControlGroup")).sendKeys(strCreditControlGroup);
		
		 * Payment
		 * Method________________________________________________________________
		 
		// driver.findElement(By.name("accounts[0].paymentMethods.paymentMethod")).sendKeys(PaymentAccountList);//tr/td[2]/select/option[text()='"+strPaymentMethod+"']")).click();
		
		 * General Bill
		 * Information______________________________________________________
		 
		driver.findElement(By.name("accounts[0].billInfo.fixedDate")).sendKeys(strDebitDate);// tr[3]/td[2]/select/option[text()='"+strDebitDate+"']")).click();
		
		 * Bill Format
		 * Information______________________________________________________
		 
		driver.findElement(By.name("accounts[0].billFormatInfo.email")).sendKeys(strEmail);
		
		 * Bill
		 * Address_________________________________________________________________
		 
		driver.findElement(By.id("accounts[0].sameAsCustomer")).click();
		*//** Navigate_____________________________________________________________________ *//*
		driver.findElement(By.id("btnNext")).click();
		Thread.sleep(1000);
		
		 * Subscriber
		 * List_______________________________________________________________
		 
		driver.findElement(By.id("btnSubAdd")).click();
		Thread.sleep(1000);
		// Check if the user selection type is available
		if (driver.findElement(By.id("sure")).getAttribute("value").matches("OK")) {
			driver.findElement(By.id("sure")).click();
		}
		
		 * Subscription
		 * Information_______________________________________________________
		 
		driver.findElement(By.name("subscriberTypeTemp")).sendKeys("Postpaid");
		driver.findElement(By.name("productId")).click();
		Thread.sleep(1000);
		 Switch to last window opened 
		// utils.PageFocus(driver);
		
		 * Product tree_______________________________________________________
		 
		String ProductID = "MP18PNPP";
		driver.switchTo().frame("topFrame");
		List<WebElement> ListProdID = driver.findElements(By.xpath("//div/input[@name='funcID']")); // Note
																									// pass
																									// the
																									// xpath
																									// of
																									// the
																									// dropdown
																									// you
																									// want
																									// to
																									// work
																									// on
		for (int i1 = 0; i1 < ListProdID.size(); i1++) {
			String outputValue = ListProdID.get(i1).getAttribute("value");
			if (outputValue.matches(ProductID)) {
				ListProdID.get(i1).click();
				break;
			}
		}
		Thread.sleep(1000);
		driver.findElement(By.xpath("//input[@value='OK']")).click();
		Thread.sleep(1000);
		 Switch to last window opened 
		// utils.PageFocus(driver);
		driver.switchTo().parentFrame();
		driver.switchTo().frame("mainFrame");
		Thread.sleep(1000);
		driver.findElement(By.name("txtMsisdn")).sendKeys("0521900272");
		// Error handling
		driver.findElement(By.name("contractPeriod")).sendKeys("24 months");
		// System.out.println(driver.findElement(By.id("salesForceID")).getAttribute("value"));
		Thread.sleep(1000);
		if ((driver.findElement(By.xpath("//tr/td[2]/input[1][@name='salesForceId'][@id='salesForceId']"))
				.getAttribute("value")).matches("Dealer Centre")) {
			driver.findElement(By.xpath("//tr/td[2]/input[1][@name='salesForceId'][@id='salesForceId']")).click();
			Thread.sleep(1000);
			// utils.Organisation(driver, strOrgID);
		}
		// utils.PageFocus(driver);
		driver.switchTo().parentFrame();
		driver.switchTo().frame("mainFrame");
		 Select Deal_______________________________________________________ 
		driver.findElement(By.name("SelectDealButton")).click();
		Thread.sleep(1000);
		 Switch to last window opened 
		// utils.PageFocus(driver);
		
		 * Deal Function_______________________________________________________
		 
		Thread.sleep(1000);
		// String DealType="Fixed Deal",Priceplan="Pinnacle
		// 3GB",DealCode="PP18PN3GPP_d100139220",IMEI="222112211111973";
		// utils.Deals(driver,"Hero Deal","Pinnacle
		// 1.5GB","PP18PN1G5PP_d100139375","222112211212155");
		// utils.PageFocus(driver);
		driver.switchTo().parentFrame();
		driver.switchTo().frame("mainFrame");
		Thread.sleep(1000);
		 Select VAS_______________________________________________________ 
		driver.findElement(By.id("selVasProd")).click();
		// utils.VASFunction(driver,"PP18PN1G5PP",ProductID);
		// utils.PageFocus(driver);
		driver.switchTo().parentFrame();
		driver.switchTo().frame("mainFrame");
		 Monthly Bill Limit________________________________________________ 
		Thread.sleep(1000);
		if ((driver.findElement(By.xpath("//h5/span/input[@name='selectVML']")).getAttribute("value"))
				.matches("Set Monthly Bill Limit")) {
			driver.findElement(By.xpath("//h5/span/input[@name='selectVML']")).click();
			Thread.sleep(1000);
			int itab = 1;
			Robot r = new Robot();
			while (itab <= 3) {
				r.keyPress(KeyEvent.VK_TAB);
				r.keyRelease(KeyEvent.VK_TAB);
				Thread.sleep(1000);
				itab++;
			}
			r.keyPress(KeyEvent.VK_ENTER);
			r.keyRelease(KeyEvent.VK_ENTER);
			// utils.MonthlyBillLimit(driver);
			Thread.sleep(1000);
			// utils.PageFocus(driver);
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
		}
		
		 * Installation Address________________________________________________
		 
		driver.findElement(By.id("installationAddress")).click();
		
		 * Installation Address same as residential address____________________
		 
		driver.findElement(By.id("installationAddressValue")).click();
		*//** SAVE_______________________________________________________________ */
		/*driver.findElement(By.id("btnSubSave")).click();
*/
		return strNorecoed;

	}

	public String BuisnessCustomerActivation(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet,
			Recordset record, ResultSet resultset, String Type, String sDefaultPath, String PPD, String location,
			String DataPath, int sheetnum) throws Exception {

		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] UsedBy = { "Used_By" };
		String strContractTem = null;
		// String strIMEIe;
		String[] NOCustomer = { "NoCustomer" };
		String dealserr = "true";
		String vaserr = "true";
		String alrtiter = "true";
		String strPaymentMethod = data.getCellData("Payment_Method", iCount, sheet, null, null, "Excel");

		String strDurationOfEmp = data.getCellData("EmpDuration", iCount, sheet, null, null, "Excel");
		String strIDNumber = data.getCellData("ID_Number", iCount, sheet, null, null, "Excel");
		String strCustomer = data.getCellData("Customer", iCount, sheet, null, null, "Excel");
		String strTypeOfActivation = data.getCellData("ActType", iCount, sheet, null, null, "Excel");
		String strIDtype = data.getCellData("ID_Type", iCount, sheet, null, null, "Excel");
		// String strIDNumber=data.getCellData("Customer",1,sheet, null,null,
		// "Excel");
		String strOrgID = data.getCellData("Sales_Force_ID", iCount, sheet, null, null, "Excel");
		String strFirstName = utils.generateRandomString(9);
		String strGender = data.getCellData("Gender", iCount, sheet, null, null, "Excel");
		String strProduct_Type = data.getCellData("Product_Type", iCount, sheet, null, null, "Excel");
		String strPricePlanId = data.getCellData("PricePlanId", iCount, sheet, null, null, "Excel");
		String strPricePlanName = data.getCellData("PricePlanName", iCount, sheet, null, null, "Excel");
		String strProductID = data.getCellData("ProductID", iCount, sheet, null, null, "Excel");
		String strProductName = data.getCellData("ProductName", iCount, sheet, null, null, "Excel");
		String strContractTerm = data.getCellData("Contract_Term", iCount, sheet, null, null, "Excel");
		String strSelectDeal = data.getCellData("Select_Deal", iCount, sheet, null, null, "Excel");
		String strmsisdn = data.getCellData("Msisdn", iCount, sheet, null, null, "Excel");
		String strsim = data.getCellData("Sim", iCount, sheet, null, null, "Excel");
		String strdelvrymtd = data.getCellData("Delivery_Format", iCount, sheet, null, null, "Excel");
		// String = data.getCellData("Contract_Term", iCount, sheet, null, null,
		// "Excel");

		// String stremail=data.getCellData("Email",iCount,sheet, null,null,
		// "Excel");
		String strConnectVas = data.getCellData("ConnectVas", iCount, sheet, null, null, "Excel");
		String strVas = data.getCellData("Vas", iCount, sheet, null, null, "Excel");
		String strSubtyp = data.getCellData("Subsribertyp", iCount, sheet, null, null, "Excel");
		String strdealerr = "true";
		String Cash = "Cash";
		String Strclienttyp = "Employee";
		String NoDispatch = "No Dispatch";
		String CompName = data.getCellData("CompName", iCount, sheet, null, null, "Excel");
		String CertificateID = data.getCellData("Certificate_ID", iCount, sheet, null, null, "Excel");

		/*if (!strContractTerm.equals("")) {
			strContractTem = Integer.parseInt(strContractTerm);
		}*/
		String strTelWork = utils.genRandnumber(8);
		strTelWork = "01" + strTelWork;
		String strCell = utils.genRandnumber(9);
		strCell = "01" + strCell;
		String strTelHome = utils.genRandnumber(8);
		strTelHome = "01" + strTelHome;
		String strBankAccType = "Savings";
		String strLastName = utils.generateRandomString(8);
		;
		String strDateOfBirth = data.getCellData("Date_Of_Birth", 1, sheet, null, null, "Excel");
		String strMonthlySalary = utils.genRandnumber(4);
		strMonthlySalary = "1" + strMonthlySalary;
		String strTypeOfResidence = "Rent";
		String strProvince = "Gauteng";
		String strRoadNumber = "1";
		String strRoadName = "Buffalo Road";
		String strSuburb = "Bronberrik";
		String strCity = "Centurion";
		String strPostCode = "0157";
		String strTitle = "Mr.";
		String AlertText;
		String vaserr1 = "true";
		String Compturnover = "1 - 5 Million";
		String CompEmpcount = "11-20 Employees";
		String strAgriculture = "Agriculture";
		String strTypeOfResident = "Tenant";
		String strDeal_Code = data.getCellData("Deal_Code", iCount, sheet, null, null, "Excel");
		String strIMEI = data.getCellData("IMEI", iCount, sheet, null, null, "Excel");
		// if(!strIMEI.equals("")){
		// strIMEIe=Long.parseLong(strIMEI);
		// }
		String strCountry = data.getCellData("Country_of_Issue", iCount, sheet, null, null, "Excel");
		String strExpiryDate = data.getCellData("Expire_date", iCount, sheet, null, null, "Excel");
		String strCompanyName = utils.generateRandomString(13);
		strCompanyName = "iLAB " + strCompanyName;
		// String PaymentAccountList=data.getCellData("Customer",1,sheet,
		// null,null, "Excel");
		String strDebitDate = data.getCellData("Debit_Date", iCount, sheet, null, null, "Excel");
		String strEmail = utils.generateRandomString(6);
		String strEmail1 = utils.genRandnumber(3);
		String strEmail3 = strEmail + strEmail1;
		String strEmail4 = strEmail3 + "@gmail.com";
		strCustomer = data.getCellData("Customer", iCount, sheet, null, null, "Excel");
		String strDiscount = data.getCellData("Discount", iCount, sheet, null, null, "Excel");

		if (strCustomer.matches("New")) {
			try {
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("topFrame");
				driver.switchTo().frame("ifraTopMenu");
				Thread.sleep(1000);
				// data.ElementExist(driver,"//a[text()='Customer
				// Care']",DataPath,iCount,sheetnum);
				/* Customer Care_______________________ */
				// Actions action = new Actions(driver);
				// action.moveToElement(driver.findElement(By.xpath("//*[@id='Customer
				// Care']"))).doubleClick().build().perform();
				driver.findElement(By.linkText("Customer Care")).click();
				driver.findElement(By.linkText("Customer Care")).click();
				Thread.sleep(500);
				/* Switch to LeftFrame */
				driver.switchTo().parentFrame();/*
												 * Note: we got to switch back
												 * to the page
												 */
				driver.switchTo().parentFrame();
				driver.switchTo().frame("leftFrame");
				Thread.sleep(800);
				// *Activation_______________________*/
				driver.findElement(By.xpath("//*[@id='div_Aanmelden_WEB']")).click();
				driver.findElement(By.xpath("//*[contains(text(),'Business Customer')]")).click();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				Thread.sleep(1000);
				if (strTypeOfActivation.matches("Upfront")) {
					driver.findElement(By.xpath("//*[@id='upfrontPaymentType']")).click();

				}
				/* Quick Vetting_____________________________ */

				if (!CompName.equals("") | null != CompName) {
					driver.findElement(By.xpath("//*[@name='m_EntityName']")).sendKeys(CompName);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "Companyname Cant be null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strTelWork.equals("") | null != strTelWork) {
					// driver.findElement(By.xpath("//*[@id='page-container']")).click();
					driver.findElement(By.xpath("//*[@name='m_LinkPhone']")).sendKeys(strTelWork);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "Buisness Tell1 Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strTelHome.equals("") | null != strTelHome) {
					driver.findElement(By.xpath("//*[@name='m_OfficeTel']")).sendKeys(strTelHome);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "Buisness Tell2 Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strCell.equals("") | null != strCell) {
					driver.findElement(By.xpath("//*[@name='m_LinkMan']")).sendKeys(strCell);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "Cell Number Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!CertificateID.equals("") | null != CertificateID) {
					driver.findElement(By.xpath("//*[@name='m_CertID']")).sendKeys(CertificateID);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "CertificateId Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.findElement(By.xpath("//select[@id='m_CompanyTurnover']/option[@value='" + 1 + "']")).click();
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("fasle")) {

					strNorecoed = "false";
					return strNorecoed;
				}
				driver.findElement(By.xpath("//select[@id='m_EmployeeCount']/option[@value='" + CompEmpcount + "']"))
						.click();
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("fasle")) {

					strNorecoed = "false";
					return strNorecoed;
				}
				driver.findElement(By.xpath("//select[@id='m_Industry']/option[@value='" + strAgriculture + "']"))
						.click();
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("fasle")) {

					strNorecoed = "false";
					return strNorecoed;
				}
				driver.findElement(By.xpath("//font[contains(text(),'Link to Address Web')]")).click();
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("fasle")) {

					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(800);
				data.PageFocus(driver);
				data.PageFocus(driver);
				driver.close();
				Thread.sleep(800);
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				driver.findElement(By.xpath("//*[@value='Retrieve Address']")).click();
				driver.findElement(By.xpath("//*[@value='Next']")).click();

				// driver.switchTo().parentFrame();
				Thread.sleep(1000);
				if (!strPaymentMethod.equals("") | null != strPaymentMethod) {
					driver.findElement(
							By.xpath("//select[@id='accounts[0].paymentMethods.paymentMethod']/option[contains(text(),'"
									+ strPaymentMethod + "')]"))
							.click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "PaymentMethod Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				/*
				 * driver.findElement(By.xpath(
				 * "//select[@name='accounts[0].paymentMethods.directDebitInfo.idDocType']/option[contains(text(),'"
				 * +strIDtype+"')]")).click(); driver.findElement(By.xpath(
				 * "//*[@name='accounts[0].paymentMethods.directDebitInfo.idDocNo']"
				 * )).sendKeys(strIDNumber); driver.findElement(By.xpath(
				 * "//select[@name='accounts[0].billFormatInfo.deliveryFormat']/option[contains(text(),'"
				 * +strdelvrymtd+"')]")).click();
				 */
				if (!strdelvrymtd.equals("") | null != strdelvrymtd) {
					driver.findElement(By
							.xpath("//select[@name='accounts[0].billFormatInfo.deliveryFormat']/option[contains(text(),'"
									+ strdelvrymtd + "')]"))
							.click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "delvrymtd Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strContractTerm.equals("") | null != strContractTerm) {
					driver.findElement(
							By.xpath("//select[@name='accounts[0].billInfo.fixedDate']/option[contains(text(),'"
									+ strContractTerm + "')]"))
							.click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "PaymentMethod Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.findElement(By.xpath("//*[@id='accounts[0].sameAsCustomer']")).click();
				driver.findElement(By.xpath("//*[@id='accounts[0].sameAsCustomer']")).click();
				Thread.sleep(600);
				driver.findElement(By.xpath("//*[@value='Next']")).click();
				Thread.sleep(1000);
				driver.findElement(By.xpath("//*[@id='btnSubAdd']")).click();

				Thread.sleep(500);
				if (!strSubtyp.equals("") | null != strSubtyp) {
					driver.findElement(By.xpath("//*[@value='" + strSubtyp + "']")).click();
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "Product type i.e mobile or FTTH ant be Null" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				// driver.findElement(By.xpath("//*[@value='"+strSubtyp+"']")).click();
				Thread.sleep(300);
				driver.findElement(By.xpath("//*[@value='OK']")).click();
				// driver.findElement(By.xpath("//*[@value='OK']")).click();

				driver.findElement(By.xpath("//*[@id='subscriberTypeTemp']/option[text()='" + strProduct_Type + "']"))
						.click();
				Thread.sleep(200);
				driver.findElement(By.xpath("//*[@id='productTree']")).click();
				Thread.sleep(1000);
				driver.switchTo().parentFrame();

				data.PageFocus(driver);
				driver.switchTo().frame("topFrame");
				strNorecoed = data.ElementExist(driver, "//div/input[@id='" + strProductID + "']", DataPath, iCount,
						sheetnum, "ProductID");
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (!strNorecoed.equals("false")) {
					driver.findElement(By.xpath("//div/input[@id='" + strProductID + "']")).click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();
					Thread.sleep(200);
					driver.findElement(By.xpath("//*[@value='OK']")).click();
					// driver.findElement(By.xpath("//*[@value='OK']")).click();

				} else {
					Thread.sleep(200);
					driver.findElement(By.xpath("//*[@value='OK']")).click();
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(200);
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if ((driver.findElement(By.xpath("//*[@id='salesForceId']")).getAttribute("value"))
						.equals("Dealer Centre")) {
					driver.findElement(By.xpath("//*[@id='salesForceId']")).click();

					Thread.sleep(1600);
					driver.switchTo().parentFrame();
					data.Organisation(driver, strOrgID);
				}
				data.PageFocus(driver);
				driver.switchTo().parentFrame();

				driver.switchTo().frame("mainFrame");
				if (!strmsisdn.equals("") | null != strmsisdn) {
					driver.findElement(By.id("txtMsisdn")).sendKeys(strmsisdn);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strmsisdnerr = { "strmsisdn  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strmsisdnerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strsim.equals("") | null != strsim) {
					driver.findElement(By.xpath("//*[@id='ssnSuffix']")).click();
					// driver.findElement(By.xpath("//*[@id='ssnSuffix']")).click();
					Thread.sleep(300);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strsimerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strsimerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strsim.equals("") | null != strsim) {
					driver.findElement(By.xpath("//*[@id='ssnSuffix']")).click();
					driver.findElement(By.xpath("//*[@id='ssnSuffix']")).sendKeys(strsim);
					Thread.sleep(300);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strsimerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strsimerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strContractTem.equals("")) {
					driver.findElement(By.xpath("//*[@id='contractPeriod']/option[@value='" + strContractTem + "']"))
							.click();
				} else {
					String[] strmsisdnerr = { "strmsisdn  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strmsisdnerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(2000);
				if (strSelectDeal.equalsIgnoreCase("SIMONLY")) {
					vaserr1 = data.BCVas(driver, strDiscount, strEmail4, strProductName, strVas, strConnectVas, iCount,
							sheetnum, DataPath, strPricePlanName);

					if (vaserr1.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}
				data.PageFocus(driver);

				driver.switchTo().frame("mainFrame");
				if (!strSelectDeal.equalsIgnoreCase("SIMONLY")) {
					strdealerr = data.BCDeal(driver, strSelectDeal, strProductID, strPricePlanId, strDeal_Code, strIMEI,
							sDefaultPath, PPD, iCount, sheetnum, DataPath);
					if (strdealerr.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");
				driver.findElement(By.xpath("//*[@name='btnSubSave']")).click();
				Thread.sleep(1000);
				Thread.sleep(500);
				WebDriverWait wait1 = new WebDriverWait(driver,
						1/* timeout in seconds */);
				wait1.until(ExpectedConditions.alertIsPresent());
				Alert alert1 = driver.switchTo().alert();
				String AlertText1 = driver.switchTo().alert().getText();
				if (AlertText1.equals(
						"For a contract buyout customer click OK to select and enter the EPIC Gift Card serial number. Click Cancel if not a contract buyout customer.")) {
					alert1.dismiss();
				} else {
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(1200);
				driver.findElement(By.xpath("//*[@value='Save and Next']")).click();
				Thread.sleep(1200);

				driver.findElement(By.xpath("//*[@id='subIndex0@_@SimFeeDiscountOtherreason']")).sendKeys("dsfdsfsd");
				// driver.findElement(By.xpath("//*[@value='Save and
				// Next']")).click();

				driver.findElement(By.xpath("//*[@value='Next']")).click();
				Thread.sleep(1200);
				driver.findElement(By.xpath("//*[@id='button_print']")).click();
				Thread.sleep(1700);
				data.PageFocus(driver);
				driver.close();
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				driver.findElement(By.xpath("//*[@id='chkGetSignedContract']")).click();

				driver.findElement(By.xpath("//*[@id='submit_reception']")).click();
				Thread.sleep(1700);
				// data.PageFocus(driver);
				// driver.close();
			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return strNorecoed;
	}

	public String DealscreationAllocation(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet,
			Recordset record, ResultSet resultset, String Type, String sDefaultPath, String PPD, String location,
			String DataPath, int sheetnum) throws Exception {
		String[] Results = { "Results" };
		String[] DealDescription = { "Deal_Description" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] UsedBy = { "Used_By" };
		int strContractTem = 0;
		// String strIMEIe;
		String[] NOCustomer = { "NoCustomer" };
		String dealserr = "true";
		String vaserr = "true";
		String alrtiter = "true";
		String strNorecord = "true";
		String strTypeChannel = "Channel Type";
		String computername = InetAddress.getLocalHost().getHostName();
		String[] computernam = { "computername" };
		// Variables
		String strDealDataType = data.getCellData("Deal_Type", iCount, sheet, null, null, "Excel");
		String strChannelType = data.getCellData("Channel_Type", iCount, sheet, null, null, "Excel");
		String strAllocateType = data.getCellData("Allocate_Type", iCount, sheet, null, null, "Excel");
		String strHandSetID = data.getCellData("Handset_ID", iCount, sheet, null, null, "Excel");
		String strPricePlanID = data.getCellData("PricePlan_ID", iCount, sheet, null, null, "Excel");
		String strPricePlanName = data.getCellData("PricePlanName", iCount, sheet, null, null, "Excel");
		String strcontractPeriod = data.getCellData("Contract_Period", iCount, sheet, null, null, "Excel");
		String strBusinessType = data.getCellData("Business_Type", iCount, sheet, null, null, "Excel");
		String strCategory = data.getCellData("Handset_Category", iCount, sheet, null, null, "Excel");
		String strBeginDate = data.getCellData("Begin_Date", iCount, sheet, null, null, "Excel");
		String strEndDate = data.getCellData("End_Date", iCount, sheet, null, null, "Excel");
		// 'Temporary
		String strvasFolder = " Others";
		String strvasItem = "Itemized Bill";
		// Const Var
		String strDealDes = utils.genRandnumber(4);
		String strDealDescription = "MasterDealAuto" + strDealDes;
		String[] DealDescriptionarr = { strDealDescription };
		String strChannelTypeTree = "FRANCHISE_NEW";

		try {

			data.PageFocus(driver);
			driver.switchTo().parentFrame();
			driver.switchTo().frame("topFrame");
			driver.switchTo().frame("ifraTopMenu");
			Thread.sleep(1000);
			// data.ElementExist(driver,"//a[text()='Customer
			// Care']",DataPath,iCount,sheetnum);
			/* Customer Care_______________________ */
			// Actions action = new Actions(driver);
			// action.moveToElement(driver.findElement(By.xpath("//*[@id='Customer
			// Care']"))).doubleClick().build().perform();
			driver.findElement(By.linkText("Product Management")).click();
			driver.findElement(By.linkText("Product Management")).click();
			Thread.sleep(500);
			/* Switch to LeftFrame */
			driver.switchTo()
					.parentFrame();/*
									 * Note: we got to switch back to the page
									 */
			driver.switchTo().parentFrame();
			driver.switchTo().frame("leftFrame");
			Thread.sleep(800);
			driver.findElement(By.xpath("//*[@id='div_Deal_Configuration_WEB']")).click();
			WebDriverWait wait = new WebDriverWait(driver,
					2 /* timeout in seconds */);
			try {
				wait.until(ExpectedConditions.alertIsPresent());
				Alert alert = driver.switchTo().alert();
				String AlertText1 = driver.switchTo().alert().getText();
				if (AlertText1.equals("The business already exists in the business area.")) {
					alert.accept();

				}
			} catch (TimeoutException eTO) {

				strNorecoed = "true";
				// return strNorecoed;
			}
			Thread.sleep(800);
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");

			driver.findElement(By.xpath("//*[@name='newBt']")).click();
			Thread.sleep(800);
			driver.switchTo().frame("salePriceInfoFrame");
			if (!strChannelType.equals("") | null != strChannelType) {

				strNorecord = data.ElementExist(driver, "//select/option[@value='" + strChannelType + "']", DataPath,
						iCount, sheetnum, "HandsetID");
				if (!strNorecord.equals("false")) {
					driver.findElement(By.xpath("//select/option[@value='" + strChannelType + "']")).click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

				} else {

					return strNorecoed;
				}

			} else {
				String[] strIDtypeerr = { "Channel Type Cant be null Please check the Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			if (!strDealDataType.equals("") | null != strDealDataType) {

				switch (strDealDataType) {
				case "Hero Deal":
					strNorecord = data.ElementExist(driver, "//select/option[text()='" + strDealDataType + "']",
							DataPath, iCount, sheetnum, "HandsetID");
					if (!strNorecord.equals("false")) {
						driver.findElement(By.xpath("//select/option[text()='" + strDealDataType + "']")).click();
						// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

					} else {

						return strNorecoed;
					}
					// driver.findElement(By.xpath("//*[@id='div_Deal_Configuration_WEB']")).click();

					break;
				case "Fixed Deal":
					strNorecord = data.ElementExist(driver, "//select/option[text()='" + strDealDataType + "']",
							DataPath, iCount, sheetnum, "HandsetID");
					if (!strNorecord.equals("false")) {
						driver.findElement(By.xpath("//select/option[text()='" + strDealDataType + "']")).click();
						// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

					} else {

						return strNorecoed;
					}

					break;
				default:
					String[] strIDtypeerr = { "Dealtype does not Exists" };
					data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
					data.WriteData(UsedBy, iCount, sheetnum, Completed, DataPath);
					strNorecord = "false";
					return strNorecord;
				}
			} else {
				String[] strIDtypeerr = { "Deal Type Cant be null Please check the Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			driver.findElement(By.xpath("//*[@id='dealDesc']")).sendKeys(strDealDescription);

			if (!strHandSetID.equals("") | null != strHandSetID) {
				driver.findElement(By.xpath("//*[@id='handSetButton']")).click();
				
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				//Thread.sleep(300);
				data.PageFocus(driver);
				breaLoop: for (int h = 1; h <= 10; h++) {
					Thread.sleep(200);
					data.PageFocus(driver);
					
					strNorecord = data.ElementExist(driver, "//select[@id='resType']/option[2]", DataPath, iCount,
							sheetnum, "HandsetID");
					if (!strNorecord.equals("false")) {
						// Thread.sleep(2000);
						driver.findElement(By.xpath("//select[@id='resType']/option[2]")).click();
						driver.findElement(By.xpath("//input[@id='resId']")).sendKeys(strHandSetID);
						driver.findElement(By.xpath("//input[@value='Search']")).click();

						// Thread.sleep(500);
						driver.switchTo().frame("goodsInfo");

						strNorecord = data.ElementExist(driver, "//input[@value='" + strHandSetID + "']", DataPath,
								iCount, sheetnum, "HandsetID");

						if (!strNorecord.equals("false")) {
							driver.findElement(By.xpath("//input[@value='" + strHandSetID + "']")).click();
							// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();
							Thread.sleep(200);
							driver.switchTo().parentFrame();
							driver.findElement(By.xpath("//input[@value=' OK']")).click();
							// driver.findElement(By.xpath("//*[@value='OK']")).click();
							break breaLoop;

						} else {

							Thread.sleep(200);
							driver.switchTo().parentFrame();
							driver.findElement(By.xpath("//input[@value=' Cancel']")).click();
							strNorecoed = "false";

							if (h == 1) {
								break breaLoop;
							}

							return strNorecoed;

						}
					} else {
						data.PageFocus(driver);
						if (h == 10) {
							String[] strIDtypeerr = { "Handsetid Cant be Null Please Check Excelsheet" };
							data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
							data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
							return strNorecoed;
						}

					}
				}
			} else {
				String[] strIDtypeerr = { "HandsetID Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			//Thread.sleep(400);
			data.PageFocus(driver);
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");
			driver.switchTo().frame("salePriceInfoFrame");

			if (!strPricePlanID.equals("") | null != strPricePlanID) {
				// driver.findElement(By.xpath("//table[@id='TheTable']/tbody/tr[3]/td[5]/input[@value='...']")).click();
				driver.findElement(By.xpath("//table[@id='TheTable']/tbody/tr[3]/td[5]/input[@value='...']")).click();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				//Thread.sleep(200);
				data.PageFocus(driver);
				breaLoop: for (int l = 1; l <= 10; l++) {
				Thread.sleep(200);
				data.PageFocus(driver);
				// Thread.sleep(1000);
				driver.switchTo().frame("productInfo");
				// Thread.sleep(500);
				strNorecord = data.ElementExist(driver, "//input[@name='queryByCode']", DataPath, iCount, sheetnum,
						"Priceplan Id Webeditbox");
				if (!strNorecord.equals("false")) {
					driver.findElement(By.xpath("//input[@name='queryByCode']")).sendKeys(strPricePlanID);
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();
					driver.findElement(By.xpath("//input[@value='Query']")).click();
					Thread.sleep(1000);
					driver.switchTo().frame("queryInfo");
					strNorecord = data.ElementExist(driver, "//input[contains(@id,'" + strPricePlanID + "')]", DataPath,
							iCount, sheetnum, "PriceplanId");
					if (!strNorecord.equals("false")) {
						// driver.findElement(By.xpath("//input[@value='"+strPricePlanID+"']")).sendKeys(strPricePlanID);
						// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();
						driver.findElement(By.xpath("//input[contains(@id,'" + strPricePlanID + "')]")).click();
						// driver.findElement(By.xpath("//*[contains(text(),'"+strPricePlanID+"')]")).click();
						driver.switchTo().parentFrame();
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value=' Save ']")).click();
						break breaLoop;

					} else {
					
						Thread.sleep(200);
						driver.switchTo().parentFrame();
						driver.findElement(By.xpath("//input[@value=' Cancel']")).click();
						String[] strIDtypeerr = { "Priceplanid Cant be Null Please Check Excelsheet" };
						data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
						data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
						
						strNorecoed = "false";
						if(l==1){
						break breaLoop;
						}
						return strNorecoed;
					}

				} else {
					
					if(l==10){
						String[] strIDtypeerr = { "Priceplanid Cant be Null Please Check Excelsheet" };
						data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
						data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
						return strNorecoed;
						
						}

					
				}
				
				}} else {
				String[] strIDtypeerr = { "Priceplanid Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			data.PageFocus(driver);
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");
			driver.switchTo().frame("salePriceInfoFrame");
			if (!strcontractPeriod.equals("") | null != strcontractPeriod) {
				strNorecord = data.ElementExist(driver,
						"//select[@id='contractPeriod']/option[contains(text(),'" + strcontractPeriod + "')]", DataPath,
						iCount, sheetnum, "HandsetID");
				if (!strNorecord.equals("false")) {
					driver.findElement(By.xpath(
							"//select[@id='contractPeriod']/option[contains(text(),'" + strcontractPeriod + "')]"))
							.click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

				} else {

					return strNorecoed;
				}

				// driver.findElement(By.xpath("//select[@id='contractPeriod']/option[contains(text(),'"+strcontractPeriod+"')]")).click();
				// driver.findElement(By.xpath("//html/body/div/div/div/div/table/tbody/tr/td[2]/select[@id='resType']/option[2]")).click();
				// driver.findElement(By.xpath("//html/body/div/div/div/div/table/tbody/tr/td[2]/select[@id='resType']/option[2]")).click();
			} else {
				String[] strIDtypeerr = { "Contract Period Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			// String = data.getCellData("Begin_Date", iCount, sheet, null,
			// null, "Excel");
			// String
			if (!strBeginDate.equals("") | null != strBeginDate) {
				driver.findElement(By.id("beginDate")).click();
				Thread.sleep(100);
				data.BirtDayFunctions(driver, strBeginDate, "BirthDate");
			} else {
				String[] strDateOfBirtherr = { "Begindate  mentioned can't be null" };
				data.WriteData(Results, iCount, sheetnum, strDateOfBirtherr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			if (!strEndDate.equals("") | null != strEndDate) {
				driver.findElement(By.xpath("//td/input[@id='endDate']")).click();

				Thread.sleep(100);
				data.BirtDayFunctions(driver, strEndDate, "ExpiryDate");
			} else {
				String[] strDateOfBirtherr = { "EndDate  mentioned can't be null" };
				data.WriteData(Results, iCount, sheetnum, strDateOfBirtherr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}

			driver.findElement(By.xpath("//input[@value='Select VAS']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("fasle")) {

				strNorecoed = "false";
				return strNorecoed;
			}
			Thread.sleep(1500);
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			data.PageFocus(driver);
			// Thread.sleep(1000);
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			Thread.sleep(1000);
			data.PageFocus(driver);
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");
			driver.switchTo().frame("salePriceInfoFrame");
			driver.findElement(By.xpath("//input[@value='Calculate']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("fasle")) {

				strNorecoed = "false";
				return strNorecoed;
			}
			Thread.sleep(1000);
			if (!strBusinessType.equals("") | null != strBusinessType) {
				strNorecord = data.ElementExist(driver,
						"//select[@id='businessType']/option[contains(text(),'" + strBusinessType + "')]", DataPath,
						iCount, sheetnum, "HandsetID");
				if (!strNorecord.equals("false")) {
					driver.findElement(
							By.xpath("//select[@id='businessType']/option[contains(text(),'" + strBusinessType + "')]"))
							.click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

				} else {

					return strNorecoed;
				}

				// driver.findElement(By.xpath("//select[@id='contractPeriod']/option[contains(text(),'"+strcontractPeriod+"')]")).click();
				// driver.findElement(By.xpath("//html/body/div/div/div/div/table/tbody/tr/td[2]/select[@id='resType']/option[2]")).click();
				// driver.findElement(By.xpath("//html/body/div/div/div/div/table/tbody/tr/td[2]/select[@id='resType']/option[2]")).click();
			} else {
				String[] strIDtypeerr = { "BusinessType Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("fasle")) {

				strNorecoed = "false";
				return strNorecoed;
			}
			Thread.sleep(2000);
			strNorecord = data.ElementExist(driver,
					"//img[contains(@src,'/custcare/images/pic/operate_success_top_en.gif')]", DataPath, iCount,
					sheetnum, "Transation was not Sucessfull");
			if (!strNorecord.equals("false")) {
				driver.findElement(By.xpath("//input[@value='OK']")).click();
				data.WriteData(DealDescription, iCount, sheetnum, DealDescriptionarr, DataPath);
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				strNorecord = DealAllocation(driver, iCount, logger, sheet, record, resultset, Type, sDefaultPath,
						"PPD", location, DataPath, sheetnum, strDealDescription);
				if (strNorecord.equals("false")) {
					return strNorecoed;
				}
				// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

			} else {
				driver.findElement(By.xpath("//input[@value='OK']")).click();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				return strNorecoed;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		return strNorecord;
	}

	public String DealAllocation(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet, Recordset record,
			ResultSet resultset, String Type, String sDefaultPath, String PPD, String location, String DataPath,
			int sheetnum, String strDealDescription) throws Exception {
		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] UsedBy = { "Used_By" };
		String[] DealCodee = { "Deal_Code" };
		String strTypeChannel = "Channel Type";
		int strContractTem = 0;
		// String strIMEIe;
		String[] NOCustomer = { "NoCustomer" };
		String dealserr = "true";
		String vaserr = "true";
		String alrtiter = "true";
		String strNorecord = "true";
		String computername = InetAddress.getLocalHost().getHostName();
		String[] computernam = { computername };
		// Variables
		String strDealDataType = data.getCellData("Deal_Type", iCount, sheet, null, null, "Excel");
		String strChannelType = data.getCellData("Channel_Type", iCount, sheet, null, null, "Excel");
		String strAllocateType = data.getCellData("Allocate_Type", iCount, sheet, null, null, "Excel");
		String strHandSetID = data.getCellData("Handset_ID", iCount, sheet, null, null, "Excel");
		String strPricePlanID = data.getCellData("PricePlan_ID", iCount, sheet, null, null, "Excel");
		String strPricePlanName = data.getCellData("PricePlanName", iCount, sheet, null, null, "Excel");
		String strcontractPeriod = data.getCellData("Contract_Period", iCount, sheet, null, null, "Excel");
		String strBusinessType = data.getCellData("Business_Type", iCount, sheet, null, null, "Excel");
		String strCategory = data.getCellData("Handset_Category", iCount, sheet, null, null, "Excel");
		String strBeginDate = data.getCellData("Begin_Date", iCount, sheet, null, null, "Excel");
		String strEndDate = data.getCellData("End_Date", iCount, sheet, null, null, "Excel");
		// String strDealDescription= data.getCellData("Deal_Description",
		// iCount, sheet, null, null, "Excel");
		// 'Temporary
		String strvasFolder = " Others";
		String strvasItem = "Itemized Bill";
		// Const Var
		// String strDealDes = utils.genRandnumber(4);
		// String strDealDescription = "MasterDealAuto" + strDealDes;
		String strChannelTypeTree = "FRANCHISE_NEW";
		String orgid = "SP2423130";
		try {
			driver.switchTo().frame("topFrame");
			driver.switchTo().frame("ifraTopMenu");
			Thread.sleep(1000);
			driver.findElement(
					By.xpath("//table[@class='topmenu_table']/tbody/tr/td[9]/a[contains(text(),'Channel Management')]"))
					.click();
			driver.findElement(By.linkText("Channel Management")).click();
			driver.findElement(By.linkText("Channel Management")).click();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().frame("leftFrame");

			driver.findElement(By.xpath("//*[@id='div_SettleRuleManage_WEB']")).click();
			driver.findElement(By.xpath("//*[contains(text(),'Manage Bonus Rate Plan')]")).click();
			driver.findElement(By.xpath("//*[@id='Allocate_Deal_WEB']")).click();
			Thread.sleep(1000);
			driver.switchTo().parentFrame();
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");
			driver.switchTo().frame("topFrame");
			driver.findElement(By.xpath("//input[@value='Add']")).click();
			Thread.sleep(2000);
			driver.switchTo().parentFrame();
			driver.switchTo().frame("contentFrame");
			driver.switchTo().frame("topAllocateFrame");
			if (!strDealDataType.equals("") | null != strDealDataType) {
				strNorecord = data.ElementExist(driver,
						"//select[@id='udctype']/option[contains(text(),'" + strDealDataType + "')]", DataPath, iCount,
						sheetnum, "HandsetID");
				if (!strNorecord.equals("false")) {
					driver.findElement(
							By.xpath("//select[@id='udctype']/option[contains(text(),'" + strDealDataType + "')]"))
							.click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

				} else {

					return strNorecoed;
				}
			} else {
				String[] strIDtypeerr = { "DealDataType Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			if (!strChannelType.equals("") | null != strChannelType) {
				strNorecord = data.ElementExist(driver,
						"//select[@id='channeltype']/option[contains(text(),'" + strChannelType + "')]", DataPath,
						iCount, sheetnum, "HandsetID");
				if (!strNorecord.equals("false")) {
					driver.findElement(
							By.xpath("//select[@id='channeltype']/option[contains(text(),'" + strChannelType + "')]"))
							.click();
					Thread.sleep(2000);
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

				} else {

					return strNorecoed;
				}
			} else {
				String[] strIDtypeerr = { "DealDataType Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}

			if (!strBeginDate.equals("") | null != strBeginDate) {
				driver.findElement(By.xpath("//td/input[@id='begindate']")).click();
				Thread.sleep(300);
				data.ChannelBirtDayFunctions(driver, strBeginDate, "BirthDate");
			} else {
				String[] strDateOfBirtherr = { "Begindate  mentioned can't be null" };
				data.WriteData(Results, iCount, sheetnum, strDateOfBirtherr, DataPath);
				data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			if (!strEndDate.equals("") | null != strEndDate) {
				driver.findElement(By.xpath("//td/input[@id='enddate']")).click();

				Thread.sleep(300);
				data.ChannelBirtDayFunctions(driver, strEndDate, "ExpiryDate");
			} else {
				String[] strDateOfBirtherr = { "EndDate  mentioned can't be null" };
				data.WriteData(Results, iCount, sheetnum, strDateOfBirtherr, DataPath);
				data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}

			driver.findElement(By.xpath("//input[@id='productname']")).click();
			// driver.findElement(By.xpath("//input[@id='productname']")).click();
			Thread.sleep(1000);
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			breakloop: for(int l=1;l<=10;l++){
			data.PageFocus(driver);
			Thread.sleep(200);
			strNorecord = data.ElementExist(driver,"//input[@name='productid']", DataPath, iCount,
					sheetnum, "PricePlanID");
			if (!strNorecord.equals("false")) {
			
			driver.findElement(By.xpath("//input[@name='productid']")).sendKeys(strPricePlanID);
			driver.findElement(By.xpath("//input[@value='Search']")).click();

			driver.findElement(By.xpath("//td[contains(text(),'" + strPricePlanID + "')]")).click();
			data.PageFocus(driver);
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");
			driver.switchTo().frame("contentFrame");
			driver.switchTo().frame("topAllocateFrame");

			driver.findElement(By.xpath("//input[@value='Search']")).click();
			// productid
			Thread.sleep(1000);
			String Totalcountofdeals = driver
					.findElement(By.xpath("//form[@name='defineDealForm']/div[2]/table/tbody/tr/td/b[3]")).getText();
			int Ttlcdeals = Integer.parseInt(Totalcountofdeals);
			if (Ttlcdeals == 0) {
				String[] strnodeals = { "No deal created for specific priceplan mentined" };
				data.WriteData(Results, iCount, sheetnum, strnodeals, DataPath);
				data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			outerloop: for (int u = 1; u <= Ttlcdeals; u++) {
				if (u > 1) {
					driver.findElement(By.xpath("//form[@name='defineDealForm']/div[2]/table/tbody/tr/td/a[3]"))
							.click();
					;
				}
				List<WebElement> deallist = driver
						.findElements(By.xpath("//form[@name='defineDealForm']/div[2]/div/table/tbody/tr")); // Note
				// on
				for (int f = 1; f <= deallist.size(); f++) {
					String dealdescrip = driver
							.findElement(By
									.xpath("//form[@name='defineDealForm']/div[2]/div/table/tbody/tr[" + f + "]/td[8]"))
							.getText();
					String dealdescriptin = dealdescrip.trim();
					if (dealdescriptin.equals(strDealDescription)) {
						String dealcod = driver
								.findElement(By.xpath(
										"//form[@name='defineDealForm']/div[2]/div/table/tbody/tr[" + f + "]/td[2]"))
								.getText();
						driver.findElement(
								By.xpath("//form[@name='defineDealForm']/div[2]/div/table/tbody/tr[" + f + "]/td[2]"))
								.click();
						String[] strdealcod = { dealcod };
						data.WriteData(DealCodee, iCount, sheetnum, strdealcod, DataPath);
                        
						break breakloop;
					}

				}
				if (u == Ttlcdeals && !dealdescriptin.equals(strDealDescription)) {
					String[] strnodealsdes = { "Dealdescription doesnot match the list found in Search" };
					data.WriteData(Results, iCount, sheetnum, strnodealsdes, DataPath);
					data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					
					return strNorecoed;
					
				}
			}}else{
				if(l==10){
					String[] strnodealsdes = { "Priceplan Webedit field doesnot match the list found in Search" };
					data.WriteData(Results, iCount, sheetnum, strnodealsdes, DataPath);
					data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
					
					
				}
			}}
			driver.findElement(By.xpath("//input[@value='Allocate']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("fasle")) {

				strNorecoed = "false";
				return strNorecoed;
			}
			Thread.sleep(1000);
			driver.switchTo().parentFrame();
			driver.switchTo().frame("contentAllocateFrame");
			Thread.sleep(500);

			if (!strChannelType.equals("") | null != strChannelType) {
				strNorecord = data.ElementExist(driver,
						"//select[@id='agenttype']/option[contains(text(),'" + strTypeChannel + "')]", DataPath, iCount,
						sheetnum, "HandsetID");
				if (!strNorecord.equals("false")) {
					driver.findElement(
							By.xpath("//select[@id='agenttype']/option[contains(text(),'" + strTypeChannel + "')]"))
							.click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

				} else {

					return strNorecoed;
				}
			} else {
				String[] strIDtypeerr = { "DealDataType Cant be Null Please Check Excelsheet" };
				data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
				data.WriteData(Completed, iCount, sheetnum, Completed, DataPath);
				strNorecoed = "false";
				return strNorecoed;
			}
			driver.findElement(
					By.xpath("//img[contains(@src,'http://ppddealerweb.cellc.co.za/channel/images/insert.gif')]"))
					.click();
			Thread.sleep(1000);
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();
			driver.switchTo().parentFrame();

			data.PageFocus(driver);
			driver.findElement(By
					.xpath("//img[@id='webfx-tree-object-4-plus'][contains(@src,'/channel/images/tree_images/xp/Lplus.png')]"))
					.click();
			Thread.sleep(1000);
			driver.findElement(By
					.xpath("//img[@id='webfx-tree-object-5-plus'][contains(@src,'/channel/images/tree_images/xp/Tplus.png')]"))
					.click();
			Thread.sleep(1000);
			driver.findElement(By.xpath("//input[@value='FRANCHISE_NEW']")).click();
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("fasle")) {

				strNorecoed = "false";
				return strNorecoed;
			}
			Thread.sleep(1000);
			// data.OrganisationDeals(driver,orgid);
			data.PageFocus(driver);
			driver.switchTo().frame("mainFrame");
			driver.switchTo().frame("rightCFrame");
			driver.switchTo().frame("tabContenIfra");
			driver.switchTo().frame("tabIframe0");
			driver.switchTo().frame("contentFrame");
			driver.switchTo().frame("contentAllocateFrame");
			driver.findElement(By.xpath("//input[@value='Save']")).click();
			alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
			if (alrtiter.equals("fasle")) {

				strNorecoed = "false";
				return strNorecoed;
			}
			Thread.sleep(2000);
			strNorecord = data.ElementExist(driver,
					"//img[contains(@src,'/channel/images/pic/operate_success_top_en.png')]", DataPath, iCount,
					sheetnum, "Transation was not Sucessfull");
			if (!strNorecord.equals("false")) {
				driver.findElement(By.xpath("//input[@value='OK']")).click();
				String[] strpass = { "Completed" };
				data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
				data.WriteData(UsedBy, iCount, sheetnum, computernam, DataPath);
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				/*
				 * DealAllocation(driver, iCount, logger, sheet, record,
				 * resultset, Type, sDefaultPath, "PPD", location, DataPath,
				 * sheetnum,strDealDescription);
				 */
				// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();

			} else {
				driver.findElement(By.xpath("//input[@value='OK']")).click();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				return strNorecoed;
			}

		} catch (Exception e) {
			// TODO: handle exception
		}

		return strNorecord;
	}

	public String ConsumercustomerActivation(WebDriver driver, int iCount, ExtentTest logger, Sheet sheet,
			Recordset record, ResultSet resultset, String Type, String sDefaultPath, String ConsumercustActivation,String PPD, String location,
			String DataPath, int sheetnum) throws Exception {

		String[] Results = { "Results" };
		String[] NORecord = { "Norecord" };
		String[] Completed = { "Completed" };
		String[] UsedBy = { "Used_By" };
		int strContractTem = 0;
		// String strIMEIe;
		String[] NOCustomer = { "NoCustomer" };
		String dealserr = "true";
		String vaserr = "true";
		String alrtiter = "true";
		String computername = InetAddress.getLocalHost().getHostName();
		String[] computernam = { "computername" };
		String strDurationOfEmp = data.getCellData("EmpDuration", iCount, sheet, null, null, "Excel");
		String strIDNumber = data.getCellData("ID_Number", iCount, sheet, null, null, "Excel");
		String strCustomer = data.getCellData("Customer", iCount, sheet, null, null, "Excel");
		String strTypeOfActivation = data.getCellData("ActType", iCount, sheet, null, null, "Excel");
		String strIDtype = data.getCellData("ID_Type", iCount, sheet, null, null, "Excel");
		// String strIDNumber=data.getCellData("Customer",1,sheet, null,null,
		// "Excel");
		String strOrgID = data.getCellData("Sales_Force_ID", iCount, sheet, null, null, "Excel");
		String strFirstName = utils.generateRandomString(9);
		String strGender = data.getCellData("Gender", iCount, sheet, null, null, "Excel");
		String strProduct_Type = data.getCellData("Product_Type", iCount, sheet, null, null, "Excel");
		String strPricePlanId = data.getCellData("PricePlanId", iCount, sheet, null, null, "Excel");
		String strPricePlanName = data.getCellData("PricePlanName", iCount, sheet, null, null, "Excel");
		String strProductID = data.getCellData("ProductID", iCount, sheet, null, null, "Excel");
		String strProductName = data.getCellData("ProductName", iCount, sheet, null, null, "Excel");
		String strContractTerm = data.getCellData("Contract_Term", iCount, sheet, null, null, "Excel");
		String strSelectDeal = data.getCellData("Select_Deal", iCount, sheet, null, null, "Excel");
		String strmsisdn = data.getCellData("Msisdn", iCount, sheet, null, null, "Excel");
		String strsim = data.getCellData("Sim", iCount, sheet, null, null, "Excel");
		// String stremail=data.getCellData("Email",iCount,sheet, null,null,
		// "Excel");
		String strConnectVas = data.getCellData("ConnectVas", iCount, sheet, null, null, "Excel");
		String strVas = data.getCellData("Vas", iCount, sheet, null, null, "Excel");
		String strSubtyp = data.getCellData("Subsribertyp", iCount, sheet, null, null, "Excel");
		String strdealerr = "true";
		String Cash = "Cash";
		String Strclienttyp = "Employee";
		String NoDispatch = "No Dispatch";
		if (!strContractTerm.equals("")) {
			strContractTem = Integer.parseInt(strContractTerm);
		}
		String strTelWork = utils.genRandnumber(8);
		strTelWork = "01" + strTelWork;
		String strCell = utils.genRandnumber(9);
		strCell = "01" + strCell;
		String strTelHome = utils.genRandnumber(8);
		strTelHome = "01" + strTelHome;
		String strBankAccType = "Savings";
		String strLastName = utils.generateRandomString(8);
		;
		String strDateOfBirth = data.getCellData("Date_Of_Birth", 1, sheet, null, null, "Excel");
		String strMonthlySalary = utils.genRandnumber(4);
		strMonthlySalary = "1" + strMonthlySalary;
		String strTypeOfResidence = "Rent";
		String strProvince = "Gauteng";
		String strRoadNumber = "1";
		String strRoadName = "Buffalo Road";
		String strSuburb = "Bronberrik";
		String strCity = "Centurion";
		String strPostCode = "0157";
		String strTitle = "Mr.";
		String AlertText;
		String vaserr1 = "true";
		String strTypeOfResident = "Tenant";
		String strDeal_Code = data.getCellData("Deal_Code", iCount, sheet, null, null, "Excel");
		String strIMEI = data.getCellData("IMEI", iCount, sheet, null, null, "Excel");
		// if(!strIMEI.equals("")){
		// strIMEIe=Long.parseLong(strIMEI);
		// }
		String strCountry = data.getCellData("Country_of_Issue", iCount, sheet, null, null, "Excel");
		String strExpiryDate = data.getCellData("Expire_date", iCount, sheet, null, null, "Excel");
		String strCompanyName = utils.generateRandomString(13);
		strCompanyName = "iLAB " + strCompanyName;
		// String PaymentAccountList=data.getCellData("Customer",1,sheet,
		// null,null, "Excel");
		String strDebitDate = data.getCellData("Debit_Date", iCount, sheet, null, null, "Excel");
		String strEmail = utils.generateRandomString(6);
		String strEmail1 = utils.genRandnumber(3);
		String strEmail3 = strEmail + strEmail1;
		String strEmail4 = strEmail3 + "@gmail.com";
		strCustomer = data.getCellData("Customer", iCount, sheet, null, null, "Excel");
		String strDiscount = data.getCellData("Discount", iCount, sheet, null, null, "Excel");

		if (strCustomer.matches("New")) {
			try {
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("topFrame");
				driver.switchTo().frame("ifraTopMenu");
				Thread.sleep(1000);
				// data.ElementExist(driver,"//a[text()='Customer
				// Care']",DataPath,iCount,sheetnum);
				/* Customer Care_______________________ */
				// Actions action = new Actions(driver);
				// action.moveToElement(driver.findElement(By.xpath("//*[@id='Customer
				// Care']"))).doubleClick().build().perform();
				/*driver.findElement(By.linkText("Customer Care")).click();
				driver.findElement(By.linkText("Customer Care")).click();*/
				utils.ClickObject(driver, "customercare", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				utils.ClickObject(driver, "customercare", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				Thread.sleep(500);
				/* Switch to LeftFrame */
				driver.switchTo().parentFrame();/*
												 * Note: we got to switch back
												 * to the page
												 */
				driver.switchTo().parentFrame();
				driver.switchTo().frame("leftFrame");
				Thread.sleep(800);
				// *Activation_______________________*/
				//driver.findElement(By.xpath("//*[@id='div_Aanmelden_WEB']")).click();
				utils.ClickObject(driver, "activation", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[contains(text(),'Consumer Customer')]")).click();
				utils.ClickObject(driver, "ConsumerCustomer", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				Thread.sleep(1000);
				if (strTypeOfActivation.matches("Normal")) {
					utils.ClickObject(driver, "creditvetting", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='creditVetting']")).click();
					// driver.findElement(By.xpath("//*[@id='telesales']")).click();
				} else {
					utils.ClickObject(driver, "creditvetting", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='creditVetting']")).click();
					//driver.findElement(By.xpath("//*[@id='upfrontPaymentType']")).click();
					utils.ClickObject(driver, "upfrontPaymentType", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				}
				/* Quick Vetting_____________________________ */

				if (!strIDtype.equals("") | null != strIDtype) {
					driver.findElement(
							By.xpath("//div[@id='quickVettingDIV']/div/table/tbody/tr/td[2]/select/option[@value='"
									+ strIDtype + "']"))
							.click();
				} else {
					String[] strIDtypeerr = { "IDType DOes not Exists" };
					data.WriteData(Results, iCount, sheetnum, strIDtypeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strIDNumber.equals("") | null != strIDNumber) {
					// driver.findElement(By.xpath("//*[@id='page-container']")).click();
					utils.EnterText(driver, "idnumber", strIDNumber,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("idNumber")).sendKeys(strIDNumber);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("fasle")) {

						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] IDNUMBER = { "IDNUMBER DOes not Exists" };
					data.WriteData(Results, iCount, sheetnum, IDNUMBER, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				
				utils.EnterText(driver, "FirstName", strIDNumber,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.id("firstName")).click();
				// driver.switchTo().parentFrame();
				Thread.sleep(500);
				WebDriverWait wait = new WebDriverWait(driver,
						1/* timeout in seconds */);
				try {
					wait.until(ExpectedConditions.alertIsPresent());
					Alert alert = driver.switchTo().alert();
					AlertText = driver.switchTo().alert().getText();
					if (AlertText
							.equals("There is an existing customer in the system, Are you sure you want to choose?")) {
						alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
						data.PageFocus(driver);
						driver.switchTo().frame("mainFrame");
						utils.ClickObject(driver, "close", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
						//driver.findElement(By.xpath("//*[text()='Close']")).click();
						//driver.findElement(By.xpath("//*[text()='Close']")).click();
						strNorecoed = "false";
						return strNorecoed;
					} else {
						alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
						if (alrtiter.equals("fasle")) {

							strNorecoed = "false";
							return strNorecoed;
						}
					}
				} catch (TimeoutException eTO) {

					strNorecoed = "true";
					// return strNorecoed;
				}
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");

				// Thread.sleep(500);
				// driver.findElement(By.id("idNumber")).sendKeys(strIDNumber);
				// driver.findElement(By.xpath("//div[@id='quickVettingDIV']/div[2]/table/tbody/tr/td")).click();
				// alrtiter=utils.GeneralAlert(driver,iCount,DataPath,sheetnum);
				// if(alrtiter.equals("false")){
				// strNorecoed="false";
				// return strNorecoed;
				// }
				/*
				 * Credit Vetting____________________
				 * ___________________________________________
				 */
				// driver.findElement(By.xpath("//*[@id='salesForceID']")).click();
				// String
				// sdgsd=driver.findElement(By.xpath("//*[@id='salesForceID']")).getAttribute("value");
				if ((driver.findElement(By.xpath("//*[@id='salesForceID']")).getAttribute("value"))
						.equals("Dealer Centre")) {
					utils.ClickObject(driver, "salesforceid", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='salesForceID']")).click();

					Thread.sleep(800);
					driver.switchTo().parentFrame();
					data.Organisation(driver, strOrgID);
				}
				data.PageFocus(driver);
				// utils.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strFirstName.equals("") | null != strFirstName) {
					//driver.findElement(By.id("firstName")).sendKeys(strFirstName);
					utils.EnterText(driver, "FirstName", strIDNumber,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strFirstNameerr = { "First name mentioned In Excel does not Exists or Cant be Null" };
					data.WriteData(Results, iCount, sheetnum, strFirstNameerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				// data.PageFocus(driver);
				// driver.switchTo().frame("mainFrame");
				if (!strGender.equals("") | null != strGender) {
					driver.findElement(By.xpath("//tr[3]/td[2]/select/option[text()='" + strGender + "']")).click();
				} else {
					String[] strGendererr = { "Gender mentioned id can't be null" };
					data.WriteData(Results, iCount, sheetnum, strGendererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strTelWork.equals("") | null != strTelWork) {
					//driver.findElement(By.xpath("//*[@id='telephoneWork']")).sendKeys(strTelWork);
					utils.EnterText(driver, "telephoneWork", strTelHome,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strGendererr = { "Work Telephone  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strGendererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strTelHome.equals("") | null != strTelHome) {
					
					utils.EnterText(driver, "TelHome", strTelHome,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.xpath("//*[@id='telephoneHome']")).sendKeys(strTelHome);
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strTelHomeerr = { "Home Telephone  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strTelHomeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strCell.equals("") | null != strCell) {
					//driver.findElement(By.xpath("//*[@id='cellphone']")).sendKeys(strCell);
					utils.EnterText(driver, "Cell", strCell,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strCellerr = { "Cellnumber  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCellerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strBankAccType.equals("") | null != strBankAccType) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strBankAccType + "']")).click();
				} else {
					String[] strBankAccTypeerr = { "BankAccType  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strBankAccTypeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strLastName.equals("") | null != strLastName) {
					utils.EnterText(driver, "LastName", strLastName,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("lastName")).sendKeys(strLastName);
					Thread.sleep(200);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strLastNameerr = { "BankAccType  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strLastNameerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");
				// DateOfBirth
				if (!strDateOfBirth.equals("") | null != strDateOfBirth) {
					utils.ClickObject(driver, "dateOfBirth", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("dateOfBirth")).click();
					Thread.sleep(100);
					data.BirtDayFunctions(driver, strDateOfBirth, "BirthDate");
				} else {
					String[] strDateOfBirtherr = { "DateOfBirth  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strDateOfBirtherr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strTypeOfResident.equals("") | null != strTypeOfResident) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strTypeOfResident + "']")).click();
				} else {
					String[] strTypeOfResidenterr = { "TypeOfResident  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strTypeOfResidenterr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				// DurationOfEmp
				if (!strDurationOfEmp.equals("") | null != strDurationOfEmp) {
					utils.ClickObject(driver, "durationofEmployment", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("durationofEmployment")).click();
					Thread.sleep(200);
					data.BirtDayFunctions(driver, strDurationOfEmp, "MonthYearDate");
				} else {
					String[] strDurationOfEmployment = { "DurationOfEmployment  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strDurationOfEmployment, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strMonthlySalary.equals("") | null != strDurationOfEmp) {
					utils.EnterText(driver, "monthlySalary", strMonthlySalary,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("monthlySalaryR")).sendKeys(strMonthlySalary);
				} else {
					String[] strMonthlySalerr = { "MonthlySalary  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strMonthlySalerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				/*
				 * Residential
				 * Address___________________________________________________________
				 */
				if (!strTypeOfResidence.equals("") | null != strTypeOfResidence) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strTypeOfResidence + "']")).click();
				} else {
					String[] strTypeOfResidenceerr = { "TypeOfResidence  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strTypeOfResidenceerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strProvince.equals("") | null != strProvince) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strProvince + "']")).click();
				} else {
					String[] strProvinceerr = { "Province  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strProvinceerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strRoadNumber.equals("") | null != strRoadNumber) {
					utils.EnterText(driver, "RoadNumber", strRoadNumber,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("streetNo")).sendKeys(strRoadNumber);
				} else {
					String[] strRoadNumbererr = { "RoadNumber  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strRoadNumbererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strRoadName.equals("") | null != strRoadName) {
					utils.EnterText(driver, "AddressLine", strRoadName,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("resAddressLine2Temp")).sendKeys(strRoadName);
				} else {
					String[] strRoadNumbererr = { "RoadName  mentioned can't be null" };
					data.WriteData(Results, iCount, sheetnum, strRoadNumbererr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strSuburb.equals("") | null != strSuburb) {
					utils.EnterText(driver, "suburb", strSuburb,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("suburb")).sendKeys(strSuburb);
				} else {
					String[] strSuburberr = { "Suburb  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strSuburberr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}

				if (!strCity.equals("") | null != strCity) {
					utils.EnterText(driver, "City", strCity,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("city")).sendKeys(strCity);
				} else {
					String[] strCityerr = { "City  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCityerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				
				if (!strPostCode.equals("") | null != strPostCode) {
					utils.EnterText(driver, "PostCode", strPostCode,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("postalCode")).sendKeys(strPostCode);
				} else {
					String[] strPostCodeerr = { "strPostCode  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strPostCodeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(1000);
				if (!strTitle.equals("") | null != strTitle) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strTitle + "']")).click();
				} else {
					String[] strPostCodeerr = { "Title can't be null" };
					data.WriteData(Results, iCount, sheetnum, strPostCodeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strCountry.equals("") | null != strCountry) {
					driver.findElement(By.xpath("//td[2]/select/option[text()='" + strCountry + "']")).click();
					// driver.switchTo().frame("mainFrame");
				} else {
					String[] strPostCodeerr = { "Country  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strPostCodeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strCountry.equals("") | null != strCountry) {
					driver.findElement(
							By.xpath("//select[@name='consumerCustInfo.clientTypeInfo.clientType']/option[text()='"
									+ Strclienttyp + "']"))
							.click();
				} else {
					String[] strPostCodeerr = { "Country  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strPostCodeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strExpiryDate.equals("") | null != strExpiryDate) {
					utils.ClickObject(driver,  "expiryDate", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("expiryDate")).click();
					Thread.sleep(1000);
					data.BirtDayFunctions(driver, strExpiryDate, "ExpiryDate");
				} else {
					String[] strExpiryDateerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strExpiryDateerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.switchTo().parentFrame();
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if (!strCompanyName.equals("") | null != strCompanyName) {
					
					utils.EnterText(driver, "companyName", strCompanyName,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.id("companyName")).sendKeys(strCompanyName);
				} else {
					String[] strCompanyNameeerr = { "Companyname  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strCompanyNameeerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				driver.findElement(By.id("next")).click();
				Thread.sleep(1000);
				driver.findElement(
						By.xpath("//*[@id='accounts[0].paymentMethods.paymentMethod']/option[text()='" + Cash + "']"))
						.click();
				/*
				 * General Bill
				 * Information______________________________________________________
				 */
				driver.findElement(
						By.xpath("//*[@id='accounts[0].billInfo.fixedDate']/option[text()='" + strDebitDate + "']"))
						.click();
				
				utils.EnterText(driver, "billformatemail", strEmail4,  sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@name='accounts[0].billFormatInfo.email']")).sendKeys(strEmail4);
				
				utils.ClickObject(driver,  "sameAsCustomer", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='accounts[0].sameAsCustomer']")).click();
				Thread.sleep(400);
				
				utils.ClickObject(driver,  "Next", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='btnNext']")).click();
				Thread.sleep(400);
				
				utils.ClickObject(driver,  "SubAdd", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='btnSubAdd']")).click();
				Thread.sleep(500);
				
				utils.ClickObject(driver,  "Subtyp", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@value='" + strSubtyp + "']")).click();
				// driver.findElement(By.xpath("//*[@value='"+strSubtyp+"']")).click();
				Thread.sleep(300);
				
				utils.ClickObject(driver,  "OK", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@value='OK']")).click();
				// driver.findElement(By.xpath("//*[@value='OK']")).click();

				driver.findElement(By.xpath("//*[@id='subscriberTypeTemp']/option[text()='" + strProduct_Type + "']"))
						.click();
				Thread.sleep(200);
				
				utils.ClickObject(driver,  "productTree", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
				//driver.findElement(By.xpath("//*[@id='productTree']")).click();
				Thread.sleep(1000);
				driver.switchTo().parentFrame();

				data.PageFocus(driver);
				driver.switchTo().frame("topFrame");
				strNorecoed = data.ElementExist(driver, "//div/input[@id='" + strProductID + "']", DataPath, iCount,
						sheetnum, "ProductID");
				if (!strNorecoed.equals("false")) {
					driver.findElement(By.xpath("//div/input[@id='" + strProductID + "']")).click();
					// driver.findElement(By.xpath("//div/input[@id='"+strProductID+"']")).click();
					Thread.sleep(200);
					utils.ClickObject(driver,  "OK", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.xpath("//*[@value='OK']")).click();
					// driver.findElement(By.xpath("//*[@value='OK']")).click();

				} else {
					Thread.sleep(200);
					utils.ClickObject(driver,  "OK", sDefaultPath + "\\Repository\\" + ConsumercustActivation + ".xml");
					//driver.findElement(By.xpath("//*[@value='OK']")).click();
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(200);
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				if ((driver.findElement(By.xpath("//*[@id='salesForceId']")).getAttribute("value"))
						.equals("Dealer Centre")) {
					driver.findElement(By.xpath("//*[@id='salesForceId']")).click();

					Thread.sleep(1600);
					driver.switchTo().parentFrame();
					data.Organisation(driver, strOrgID);
				}
				data.PageFocus(driver);
				driver.switchTo().parentFrame();

				// utils.PageFocus(driver);

				driver.switchTo().frame("mainFrame");
				if (!strmsisdn.equals("") | null != strmsisdn) {
					driver.findElement(By.id("txtMsisdn")).sendKeys(strmsisdn);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strmsisdnerr = { "strmsisdn  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strmsisdnerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strsim.equals("") | null != strsim) {
					driver.findElement(By.xpath("//*[@id='ssnSuffix']")).click();
					// driver.findElement(By.xpath("//*[@id='ssnSuffix']")).click();
					Thread.sleep(300);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strsimerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strsimerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (!strsim.equals("") | null != strsim) {
					driver.findElement(By.xpath("//*[@id='ssnSuffix']")).click();
					driver.findElement(By.xpath("//*[@id='ssnSuffix']")).sendKeys(strsim);
					Thread.sleep(300);
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				} else {
					String[] strsimerr = { "sim  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strsimerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				if (strContractTem != 0) {
					driver.findElement(By.xpath("//*[@id='contractPeriod']/option[@value='" + strContractTem + "']"))
							.click();
				} else {
					String[] strmsisdnerr = { "strmsisdn  can't be null" };
					data.WriteData(Results, iCount, sheetnum, strmsisdnerr, DataPath);
					data.WriteData(Results, iCount, sheetnum, Completed, DataPath);
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(2000);
				if (strSelectDeal.equalsIgnoreCase("SIMONLY")) {
					vaserr1 = data.BCVas(driver, strDiscount, strEmail4, strProductName, strVas, strConnectVas, iCount,
							sheetnum, DataPath, strPricePlanName);

					if (vaserr1.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}
				data.PageFocus(driver);

				driver.switchTo().frame("mainFrame");
				if (!strSelectDeal.equalsIgnoreCase("SIMONLY")) {
					strdealerr = data.BCDeal(driver, strSelectDeal, strProductID, strPricePlanId, strDeal_Code, strIMEI,
							sDefaultPath, PPD, iCount, sheetnum, DataPath);
					if (strdealerr.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}
				data.PageFocus(driver);
				driver.switchTo().frame("mainFrame");
				driver.findElement(By.xpath("//*[@name='btnSubSave']")).click();
				Thread.sleep(1000);
				Thread.sleep(500);
				WebDriverWait wait1 = new WebDriverWait(driver,
						1/* timeout in seconds */);
				wait1.until(ExpectedConditions.alertIsPresent());
				Alert alert1 = driver.switchTo().alert();
				String AlertText1 = driver.switchTo().alert().getText();
				if (AlertText1.equals(
						"For a contract buyout customer click OK to select and enter the EPIC Gift Card serial number. Click Cancel if not a contract buyout customer.")) {
					alert1.dismiss();
				} else {
					alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
					if (alrtiter.equals("false")) {
						strNorecoed = "false";
						return strNorecoed;
					}
				}
				alrtiter = utils.GeneralAlert(driver, iCount, DataPath, sheetnum);
				if (alrtiter.equals("false")) {
					strNorecoed = "false";
					return strNorecoed;
				}
				Thread.sleep(1200);
				driver.findElement(By.xpath("//*[@value='Save and Next']")).click();
				Thread.sleep(1200);

				driver.findElement(By.xpath("//*[@id='subIndex0@_@SimFeeDiscountOtherreason']")).sendKeys("dsfdsfsd");
				// driver.findElement(By.xpath("//*[@value='Save and
				// Next']")).click();

				driver.findElement(By.xpath("//*[@value='Next']")).click();
				Thread.sleep(1200);
				driver.findElement(By.xpath("//*[@id='button_print']")).click();
				Thread.sleep(1700);
				data.PageFocus(driver);
				driver.close();
				data.PageFocus(driver);
				driver.switchTo().parentFrame();
				driver.switchTo().frame("mainFrame");
				driver.findElement(By.xpath("//*[@id='chkGetSignedContract']")).click();

				driver.findElement(By.xpath("//*[@id='submit_reception']")).click();
				Thread.sleep(1700);
				data.PageFocus(driver);
				driver.close();

			} catch (Exception e) {
				// TODO: handle exception
			}
		}

		return strNorecoed;
	}

}
/*	*//*****************************************************************************
		 * Function Name: appSearchHotel Description: Search Available Hotels
		 * Date Created: 13/09/2017
		 * 
		 * @throws Exception
		 ******************************************************************************/
/*
 * 
 * public void appSelectHotel(WebDriver driver) { if
 * (utils.checkIfObjectDisplayedByID(driver, "continue")) {
 * utils.ClickObjectByID(driver, "radiobutton_0"); utils.ClickObjectByID(driver,
 * "continue"); } }
 * 
 *//*****************************************************************************
	 * Function Name: appBookHotel Description: Search Available Hotels Date
	 * Created: 13/09/2017
	 * 
	 * @param sheet
	 * @param sheet
	 * @param record
	 * @param Type
	 * @param sDefaultPath
	 * @throws Exception
	 ******************************************************************************//*
																					 * public
																					 * String
																					 * [
																					 * ]
																					 * appBookHotel
																					 * (
																					 * WebDriver
																					 * driver,
																					 * int
																					 * iCount,
																					 * ExtentTest
																					 * logger,
																					 * Sheet
																					 * sheet,
																					 * Recordset
																					 * record
																					 * ,
																					 * ResultSet
																					 * resultset,
																					 * String
																					 * Type,
																					 * String
																					 * sDefaultPath)
																					 * throws
																					 * Exception
																					 * {
																					 * String
																					 * []
																					 * sOrderDetails
																					 * =
																					 * new
																					 * String
																					 * [
																					 * 2
																					 * ]
																					 * ;
																					 * if
																					 * (utils
																					 * .
																					 * checkIfObjectDisplayedByID
																					 * (
																					 * driver,
																					 * "book_now"
																					 * )
																					 * )
																					 * {
																					 * 
																					 * String
																					 * CreditCardNumber
																					 * =
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "CreditCardNumber"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * EnterTextByID
																					 * (
																					 * driver,
																					 * "first_name",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "FirstName"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * EnterTextByID
																					 * (
																					 * driver,
																					 * "last_name",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "LastName"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * EnterTextByID
																					 * (
																					 * driver,
																					 * "address",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "BillingAddress"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * EnterTextByID
																					 * (
																					 * driver,
																					 * "cc_num",
																					 * CreditCardNumber
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * SelectTextByIDUsingValue
																					 * (
																					 * driver,
																					 * "cc_type",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "CreditCardType"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * SelectTextByIDUsingVisibleText
																					 * (
																					 * driver,
																					 * "cc_exp_month",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "ExpiryDateMonth"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * SelectTextByIDUsingVisibleText
																					 * (
																					 * driver,
																					 * "cc_exp_year",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "ExpiryDateYear"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * EnterTextByID
																					 * (
																					 * driver,
																					 * "cc_cvv",
																					 * data
																					 * .
																					 * getCellData
																					 * (
																					 * "CVVNumber"
																					 * ,
																					 * iCount
																					 * ,
																					 * sheet,
																					 * record
																					 * ,
																					 * resultset,
																					 * Type
																					 * )
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * ClickObjectByID
																					 * (
																					 * driver,
																					 * "book_now"
																					 * )
																					 * ;
																					 * utils
																					 * .
																					 * waitforPropertyByID
																					 * (
																					 * driver,
																					 * "order_no"
																					 * ,
																					 * 15
																					 * )
																					 * ;
																					 * if
																					 * (utils
																					 * .
																					 * checkIfObjectDisplayedByID
																					 * (
																					 * driver,
																					 * "order_no"
																					 * )
																					 * )
																					 * {
																					 * String
																					 * sOrderNo
																					 * =
																					 * utils
																					 * .
																					 * GetAttributeValueByID
																					 * (
																					 * driver,
																					 * "order_no"
																					 * ,
																					 * "value"
																					 * )
																					 * ;
																					 * sOrderDetails
																					 * [
																					 * 0]
																					 * =
																					 * sOrderNo;
																					 * sOrderDetails
																					 * [
																					 * 1]
																					 * =
																					 * CreditCardNumber;
																					 * }
																					 * utils
																					 * .
																					 * ExtentLogPassFailByID
																					 * (
																					 * driver,
																					 * "order_no",
																					 * "Hotel was booked successfully: "
																					 * +sOrderDetails
																					 * [
																					 * 0
																					 * ]
																					 * ,
																					 * "Hotel booking unSuccessful see screenshot in the report"
																					 * ,
																					 * logger,
																					 * true,
																					 * sDefaultPath
																					 * )
																					 * ;
																					 * 
																					 * }
																					 * return
																					 * sOrderDetails;
																					 * 
																					 * }
																					 * }
																					 */
