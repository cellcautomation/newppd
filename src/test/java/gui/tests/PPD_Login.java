package gui.tests;

import org.testng.annotations.Test;
import org.testng.annotations.BeforeClass;
import org.testng.Assert;
import java.util.List;
import java.sql.ResultSet; 
import org.apache.poi.ss.usermodel.Sheet;
import org.openqa.selenium.WebDriver;
import org.testng.AssertJUnit;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.codoid.products.fillo.Recordset;
import gui.Functions.ApplicationSpecific;
import gui.Functions.DataFunctions;
import gui.Functions.UtilityFunctions;  

  
public class PPD_Login {
	static UtilityFunctions utils = new UtilityFunctions();
	static DataFunctions data = new DataFunctions();
	static String[] env=new String[10];
	ExtentTest logger;
	static  ExtentReports extent; 
	int iRow;
	Sheet sheet; 
	Recordset record;
	static String sDefaultPath;
	ResultSet resultset; 
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		
		sDefaultPath = System.getProperty("user.dir");
		sDefaultPath = sDefaultPath.replace("batch", ""); 
		utils.WindowsProcess("IEDriverServer.exe");
		utils.CloseRunningProcess();
		utils.WindowsProcess("chromedriver.exe");
		utils.CloseRunningProcess();
		utils.WindowsProcess("geckodriver.exe");
		utils.CloseRunningProcess();
		data.GetEnvironmentVariables(sDefaultPath,"PPDEnvironment");
		 extent = utils.initializeExtentReports("PPD", sDefaultPath);
	}

	@SuppressWarnings("deprecation")
	@Test
	//test build1
	public void test() {
		try
		{
			
		String sLocation = data.getDataLocation();
		String sDataType = data.getDataType();
		String sDBHost = data.getDBHost();
		String sDBUsername = data.getDBUsername();
		String sDBPassword = data.getDBPassword();	
		logger = extent.createTest("PPD");
		logger.assignCategory("Regression");
		logger.assignAuthor("Elis Shaik");
		ApplicationSpecific app = new ApplicationSpecific();
		sheet = data.ReadExcel(sLocation+"PPDExcel.xls",0);
		String location =sLocation+"PPDExcel.xls";
		WebDriver driver = utils.initializeWedriver(data.getCellData("Browser",1,sheet, null,null, "Excel"), sDefaultPath);
		utils.navigate(driver, data.getWebURL()); 

	app.appLoginAdactin(driver,data.getWebUserName(),data.getWebPassword(), logger,sDefaultPath,"PPD");
	
		//connect and get count the number of rows EXCEL, FILLO and DB(ORACLE, SQL)
		switch (data.getDataType().toUpperCase())
		{ 

			case "EXCEL": sheet = data.ReadExcel(sLocation+"PPDExcel.xls",0);
						iRow = data.rowCount(sheet, record, resultset, sDataType)-1;
						 break;
	
			case "FILLO": record = data.ConnectFillo(sLocation+"DataExcel.xlsx","Select * from Sheet1");
					
						 iRow = data.rowCount(sheet, record, resultset, sDataType);
						 break;
						 
			case "SQLSERVER":resultset = data.ConnectAndQuerySQLServer(sDBHost, sDBUsername,sDBPassword, "Select * from  [BookFlights].[dbo].[BookFlights]");
						 iRow = data.rowCount(sheet, record, resultset, sDataType);
						 resultset = data.ConnectAndQuerySQLServer(sDBHost, sDBUsername,sDBPassword, "Select * from  [BookFlights].[dbo].[BookFlights]");
		}
		
		
		String sStatus = null;
		//Book flight and write the order number back to the specified data type
		for (int i = 1 ;i<=iRow;i++)
		{
			if(i > 1) 
			{
				if (data.getCellData("Browser",i,sheet, null,null, "Excel").contains(data.getCellData("Browser",i-1,sheet, null,null, "Excel")) == false)
				{
					driver.quit();
					driver = utils.initializeWedriver(data.getCellData("Browser",i,sheet, null,null, "Excel"), sDefaultPath);
					utils.navigate(driver, data.getWebURL());
					app.appLoginAdactin(driver,data.getWebUserName(),data.getWebPassword(), logger,sDefaultPath,"PPD");
				}
			}
			
			//app.appSearchHotel(driver,i, logger, sheet,record,resultset,sDataType,sDefaultPath,"PPD",location,sheetnum);
		//	app.appSelectHotel(driver);
			//String[] OrderNum = app.appBookHotel(driver,i, logger, sheet,record,resultset,sDataType,sDefaultPath);
		/*	
			String[] Column = {"OrderNumber"};
			String[] Data = {OrderNum[0]};
			
			if (OrderNum[0] != null && OrderNum[0] != "")
			{
				sStatus = sStatus+ "Passed";
				System.out.println("Order number generated: "+OrderNum[0]);
				String updateTableSQL = null;
				switch (sDataType.toUpperCase())
				{
				
				case "SQLSERVER":
					updateTableSQL = "UPDATE [BookFlights].[dbo].[BookFlights]"
						+ " SET OrderNumber = '"+OrderNum[0]+"' "
						+ " WHERE CreditCardNumber = '"+OrderNum[1]+"'";
				
				break;
				case "FILLO":
					 updateTableSQL = "UPDATE Sheet1"
							+ " SET OrderNumber = '"+OrderNum[0]+"' "
							+ " WHERE CreditCardNumber = '"+OrderNum[1]+"'";
					
					break;
				//test Excell
				case "EXCEL":
				
				data.writeData(Column, i, Data, sLocation+"DataExcel.xlsx", sDataType, updateTableSQL);
				}
			}else{System.out.println("No order was generated check the report for details");sStatus = sStatus+ "Failed";}*/
		}
		
		
		
		extent.flush();
		//driver.quit();
		//send email report via email
		if (Boolean.parseBoolean(data.getSendEmail()))
		{
			utils.SendEmail(data.getEmailFrom(), data.getEmailTo(), "PPD",sDefaultPath );
		}
		
		
		driver.quit();
		if (sStatus.contains("Failed"))
		{
			Assert.fail();
		}
		}
		catch(Exception e)
		{
			logger.fail(e);
			System.out.print(e.getMessage());
		}
	}

}


